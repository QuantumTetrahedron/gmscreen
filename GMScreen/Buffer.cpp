#include "Buffer.h"
#include "CommandPool.h"

VkBuffer Buffer::get() const
{
	return buffer;
}

VkDeviceMemory Buffer::getMemory() const
{
	return memory;
}

void Buffer::copy(Buffer& src, VkDeviceSize size)
{
	auto command = CommandPool::beginSingleTimeCommands();

	VkBufferCopy copyRegion{};
	copyRegion.srcOffset = 0;
	copyRegion.dstOffset = 0;
	copyRegion.size = size;
	vkCmdCopyBuffer(command, src.get(), buffer, 1, &copyRegion);

	CommandPool::endSingleTimeCommands(command);
}

void Buffer::create(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties)
{
	if (size == 0) {
		return;
	}

	VkBufferCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	createInfo.size = size;
	createInfo.usage = usage;
	createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	vkCreateBuffer(VulkanLoader::GetDevice(), &createInfo, nullptr, &buffer);

	VkMemoryRequirements memRequirements;
	vkGetBufferMemoryRequirements(VulkanLoader::GetDevice(), buffer, &memRequirements);

	VkMemoryAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = VulkanLoader::findMemoryType(memRequirements.memoryTypeBits, properties);
	vkAllocateMemory(VulkanLoader::GetDevice(), &allocInfo, nullptr, &memory);

	vkBindBufferMemory(VulkanLoader::GetDevice(), buffer, memory, 0);
}

void Buffer::cleanup()
{
	vkDestroyBuffer(VulkanLoader::GetDevice(), buffer, nullptr);
	vkFreeMemory(VulkanLoader::GetDevice(), memory, nullptr);
}
