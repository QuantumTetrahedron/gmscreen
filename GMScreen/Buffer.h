#pragma once
#include "VulkanLoader.h"

class Buffer
{
public:
	VkBuffer get() const;
	VkDeviceMemory getMemory() const;

	void create(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties);
	void cleanup();

	void copy(Buffer& src, VkDeviceSize size);
private:
	friend class Device;

	VkBuffer buffer;
	VkDeviceMemory memory;
};