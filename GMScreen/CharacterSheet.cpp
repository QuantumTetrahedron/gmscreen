#include "CharacterSheet.h"
#include "ResourcesManager.h"
#include "ImGUI/imgui_stdlib.h"

LuaReader CharacterSheet::lua;
std::shared_ptr<Player> CharacterSheet::currentCharacter;

std::string CharacterSheet::luaConfig;

CharacterSheet::CharacterSheet(std::string name, int uid) : Window(name, uid)
{
	startDocked = true;
	SetWindowFlags(GetWindowFlags() | ImGuiWindowFlags_NoSavedSettings);
	SetPlayer(name);
}

void CharacterSheet::DrawContent()
{
	ImGui::InputText("Name", &ch->name);

	ImGui::Separator();

	currentCharacter = ch;

	if (luaConfig != "") {
		lua.Read(luaConfig.c_str());
	}
	else {
		ImGui::Text("Description");
		if (currentCharacter->data.find("description") == currentCharacter->data.end()) {
			currentCharacter->data["description"] = std::string("No character sheet file selected.\nFor more options, choose a character sheet config in the campaign settings.");
		}
		ImGui::InputTextMultiline("##description", &currentCharacter->get<std::string>("description"));
	}

	currentCharacter = nullptr;
}

void CharacterSheet::SetPlayer(std::string playerName)
{
	for (auto& player : ResourcesManager::players) {
		if (player->name == playerName) {
			ch = player;
			return;
		}
	}
	ch = nullptr;
}

void CharacterSheet::SetConfigFile(std::string filePath)
{
	luaConfig = filePath;
	lua.Register("inputText", inputText);
	lua.Register("inputTextMultiline", inputTextMultiline);
	lua.Register("inputInt", inputInt);
	lua.Register("inputFloat", inputFloat);
	lua.Register("displayText", displayText);
	lua.Register("displayTextMultiline", displayTextMultiline);
	lua.Register("displayInt", displayInt);
	lua.Register("displayFloat", displayFloat);
	lua.Register("setText", setText);
	lua.Register("setInt", setInt);
	lua.Register("setFloat", setFloat);
	lua.Register("setBoolean", setBoolean);
	lua.Register("getText", getText);
	lua.Register("getInt", getInt);
	lua.Register("getFloat", getFloat);
	lua.Register("getBoolean", getBoolean);
	lua.Register("checkbox", checkbox);
	lua.Register("combo", combo);
	lua.Register("selectable", selectable);
	lua.Register("button", button);
	lua.Register("collapsingHeader", collapsingHeader);
	lua.Register("separator", separator);
	lua.Register("sameLine", sameLine);
	lua.Register("text", text);
	lua.Register("dummy", dummy);
	lua.Register("indent", indent);
	lua.Register("unindent", unindent);
	lua.Register("beginGroup", beginGroup);
	lua.Register("endGroup", endGroup);
	lua.Register("beginChild", beginChild);
	lua.Register("endChild", endChild);
	lua.Register("setNextItemWidth", setNextItemWidth);
	lua.Register("getContentRegion", getContentRegion);
	lua.Register("textValue", textValue);
	lua.Register("intValue", intValue);
	lua.Register("floatValue", floatValue);
	lua.Register("booleanVallue", booleanValue);
	lua.Register("vecSize", vecSize);
	lua.Register("vecGetText", vecGetText);
	lua.Register("vecGetInt", vecGetInt);
	lua.Register("vecGetFloat", vecGetFloat);
	lua.Register("vecGetBoolean", vecGetBoolean);
	lua.Register("vecAddText", vecAddText);
	lua.Register("vecAddInt", vecAddInt);
	lua.Register("vecAddFloat", vecAddFloat);
	lua.Register("vecAddBoolean", vecAddBoolean);
	lua.Register("vecErase", vecErase);
	lua.Register("inputVecText", inputVecText);
	lua.Register("inputVecTextMultiline", inputVecTextMultiline);
	lua.Register("inputVecInt", inputVecInt);
	lua.Register("inputVecFloat", inputVecFloat);
	lua.Register("inputVecBoolean", inputVecBoolean);
	lua.Register("vecSetText", vecSetText);
	lua.Register("vecSetInt", vecSetInt);
	lua.Register("vecSetFloat", vecSetFloat);
	lua.Register("vecSetBoolean", vecSetBoolean);
	lua.Register("vecCombo", vecCombo);
}

bool CharacterSheet::assertArgsNum(lua_State* l, int min, int max)
{
	int n = lua_gettop(l);
	if (n < min || n > max) {
		std::cout << "wrong args count" << std::endl;
		return true;
	}
	return false;
}

const char* CharacterSheet::getLuaString(lua_State* l, int index, const char* d)
{
	if (lua_isstring(l, index)) {
		return lua_tostring(l, index);
	}
	return d;
}

int CharacterSheet::getLuaInt(lua_State* l, int index, int d)
{
	if (lua_isinteger(l, index)) {
		return lua_tointeger(l, index);
	}
	return d;
}

float CharacterSheet::getLuaFloat(lua_State* l, int index, float d)
{
	if (lua_isnumber(l, index)) {
		return lua_tonumber(l, index);
	}
	return d;
}

bool CharacterSheet::getLuaBoolean(lua_State* l, int index, bool d)
{
	if (lua_isboolean(l, index)) {
		return lua_toboolean(l, index);
	}
	return d;
}

// bool inputText(string name, string initial = "", int flags = 0)
int CharacterSheet::inputText(lua_State* l)
{
	if (assertArgsNum(l, 1, 3)) {
		lua_pushboolean(l, false);
		return 1;
	}

	const char* name = getLuaString(l, 1);
	const char* initial = getLuaString(l, 2);
	int flags = getLuaInt(l, 3);

	std::string& v = currentCharacter->get<std::string>(name, initial);

	bool ret = ImGui::InputText(name, &v, flags);
	lua_pushboolean(l, ret);
	return 1;
}

// bool inputTextMultiline(string name, string initial = "", float w = 0, float h = 0, int flags = 0)
int CharacterSheet::inputTextMultiline(lua_State* l)
{
	if (assertArgsNum(l, 1, 5)) {
		lua_pushboolean(l, false);
		return 1;
	}

	const char* name = getLuaString(l, 1);
	const char* initial = getLuaString(l, 2);
	float w = getLuaFloat(l, 3);
	float h = getLuaFloat(l, 4);
	int flags = getLuaInt(l, 5);

	std::string& v = currentCharacter->get<std::string>(name, initial);

	bool ret = ImGui::InputTextMultiline(name, &v, ImVec2(w, h), flags);
	lua_pushboolean(l, ret);
	return 1;
}

// bool inputInt(string name, int initial = 0, float speed = 1.0f, int min = 0, int max = 0, string format = "%d", int flags = 0)
int CharacterSheet::inputInt(lua_State* l)
{
	if (assertArgsNum(l, 1, 7)) {
		lua_pushboolean(l, false);
		return 1;
	}

	const char* name = getLuaString(l, 1);
	int initial = getLuaInt(l, 2);
	float speed = getLuaFloat(l, 3, 1.0f);
	int min = getLuaInt(l, 4);
	int max = getLuaInt(l, 5);
	const char* format = getLuaString(l, 6, "%d");
	int flags = getLuaInt(l, 7);

	int& v = currentCharacter->get<int>(name, initial);
	bool ret = ImGui::DragInt(name, &v, speed, min, max, format, flags);
	lua_pushboolean(l, ret);
	return 1;
}

// bool inputFloat(string name, float initial = 0.0f, float speed = 1.0f, float min = 0.0f, float max = 0.0f, string format = "%d", int flags = 0)
int CharacterSheet::inputFloat(lua_State* l)
{
	if (assertArgsNum(l, 1, 7)) {
		lua_pushboolean(l, false);
		return 1;
	}

	const char* name = getLuaString(l, 1);
	float initial = getLuaFloat(l, 2);
	float speed = getLuaFloat(l, 3, 1.0f);
	float min = getLuaFloat(l, 4);
	float max = getLuaFloat(l, 5);
	const char* format = getLuaString(l, 6, "%d");
	int flags = getLuaInt(l, 7);

	float& v = currentCharacter->get<float>(name, initial);
	bool ret = ImGui::DragFloat(name, &v, speed, min, max, format, flags);
	lua_pushboolean(l, ret);
	return 1;
}

// void displayText(string name, string value)
int CharacterSheet::displayText(lua_State* l)
{
	if (assertArgsNum(l, 2, 2)) {
		return 0;
	}

	const char* name = getLuaString(l, 1);
	std::string value = std::string(getLuaString(l, 2));
	
	ImGui::InputText(name, &value, ImGuiInputTextFlags_ReadOnly);
	return 0;
}

// void displayTextMultiline(string name, string value, int w = 0, int h = 0)
int CharacterSheet::displayTextMultiline(lua_State* l)
{
	if (assertArgsNum(l, 2, 4)) {
		return 0;
	}

	const char* name = getLuaString(l, 1);
	std::string value = std::string(getLuaString(l, 2));
	int w = getLuaInt(l, 3);
	int h = getLuaInt(l, 4);
	ImGui::InputTextMultiline(name, &value, ImVec2(w, h), ImGuiInputTextFlags_ReadOnly);
	return 0;
}

// void displayInt(string name, int value, string format = "%d")
int CharacterSheet::displayInt(lua_State* l)
{
	if (assertArgsNum(l, 2, 3)) {
		return 0;
	}
	const char* name = getLuaString(l, 1);
	int value = getLuaInt(l, 2);
	const char* format = getLuaString(l, 3, "%d");
	ImGui::DragInt(name, &value, 0.0f, 0, 0, format, ImGuiSliderFlags_NoInput);
	return 0;
}

// void displayFloat(string name, float value, string format = "%d")
int CharacterSheet::displayFloat(lua_State* l)
{
	if (assertArgsNum(l, 2, 3)) {
		return 0;
	}
	const char* name = getLuaString(l, 1);
	float value = getLuaFloat(l, 2);
	const char* format = getLuaString(l, 3, "%d");
	ImGui::DragFloat(name, &value, 0.0f, 0, 0, format, ImGuiSliderFlags_NoInput);
	return 0;
}

// void setText(string name, string value)
int CharacterSheet::setText(lua_State* l)
{
	if (assertArgsNum(l, 2, 2)) {
		return 0;
	}
	const char* name = getLuaString(l, 1);
	const char* value = getLuaString(l, 2);
	currentCharacter->data[name] = std::string(value);
	return 0;
}

// void setInt(string name, int value)
int CharacterSheet::setInt(lua_State* l)
{
	if (assertArgsNum(l, 2, 2)) {
		return 0;
	}
	const char* name = getLuaString(l, 1);
	int value = getLuaInt(l, 2);
	currentCharacter->data[name] = value;
	return 0;
}

// void setFloat(string name, float value)
int CharacterSheet::setFloat(lua_State* l)
{
	if (assertArgsNum(l, 2, 2)) {
		return 0;
	}
	const char* name = getLuaString(l, 1);
	float value = getLuaFloat(l, 2);
	currentCharacter->data[name] = value;
	return 0;
}

// void setBoolean(string name, bool value)
int CharacterSheet::setBoolean(lua_State* l)
{
	if (assertArgsNum(l, 2, 2)) {
		return 0;
	}
	const char* name = getLuaString(l, 1);
	bool value = getLuaBoolean(l, 2);
	currentCharacter->data[name] = value;
	return 0;
}

// string getText(string name)
int CharacterSheet::getText(lua_State* l)
{
	if (assertArgsNum(l, 1, 1)) {
		lua_pushstring(l, "");
		return 1;
	}
	const char* name = getLuaString(l, 1);
	std::string& s = currentCharacter->get<std::string>(name, "");
	lua_pushstring(l, s.c_str());
	return 1;
}

// int getInt(string name)
int CharacterSheet::getInt(lua_State* l)
{
	if (assertArgsNum(l, 1, 1)) {
		lua_pushinteger(l, 0);
		return 1;
	}
	const char* name = getLuaString(l, 1);
	int& v = currentCharacter->get<int>(name, 0);
	lua_pushinteger(l, v);
	return 1;
}

// float getFloat(string name)
int CharacterSheet::getFloat(lua_State* l)
{
	if (assertArgsNum(l, 1, 1)) {
		lua_pushnumber(l, 0.0f);
		return 1;
	}
	const char* name = getLuaString(l, 1);
	float& v = currentCharacter->get<float>(name, 0.0f);
	lua_pushnumber(l, v);
	return 1;
}

// bool getBoolean(string name)
int CharacterSheet::getBoolean(lua_State* l)
{
	if (assertArgsNum(l, 1, 1)) {
		lua_pushboolean(l, false);
		return 1;
	}
	const char* name = getLuaString(l, 1);
	bool& v = currentCharacter->get<bool>(name, false);
	lua_pushboolean(l, v);
	return 1;
}

// bool checkbox(string name)
int CharacterSheet::checkbox(lua_State* l)
{
	if (assertArgsNum(l, 1, 1)) {
		lua_pushboolean(l, false);
		return 1;
	}
	const char* name = getLuaString(l, 1);
	bool& v = currentCharacter->get<bool>(name, false);
	bool ret = ImGui::Checkbox(name, &v);
	lua_pushboolean(l, ret);
	return 1;
}

// bool combo(string name, int initial, int argCount, ...string values)
int CharacterSheet::combo(lua_State* l)
{
	int n = lua_gettop(l);
	if (n < 4) {
		std::cout << "wrong args count" << std::endl;
		lua_pushboolean(l, false);
		return 1;
	}

	const char* name = getLuaString(l, 1);
	int initial = getLuaInt(l, 2);
	int count = getLuaInt(l, 3);

	if (initial < 0) initial = 0;
	if (initial >= count) initial = count - 1;

	std::vector<const char*> values;
	for (int i = 0; i < count; ++i) {
		values.push_back(getLuaString(l, i + 4));
	}

	int& v = currentCharacter->get<int>(name, initial);
	bool ret = ImGui::Combo(name, &v, values.data(), count);
	lua_pushboolean(l, ret);
	return 1;
}

// bool selectable(string name, bool selected, int flags = 0, float w = 0, float h = 0)
int CharacterSheet::selectable(lua_State* l)
{
	if (assertArgsNum(l, 2, 5)) {
		lua_pushboolean(l, false);
		return 1;
	}

	const char* name = getLuaString(l, 1);
	bool selected = getLuaBoolean(l, 2);
	int flags = getLuaInt(l, 3);
	float w = getLuaFloat(l, 4);
	float h = getLuaFloat(l, 5);

	bool ret = ImGui::Selectable(name, selected, flags, ImVec2(w, h));
	lua_pushboolean(l, ret);
	return 1;
}

// bool button(string name, float w = 0.0f, float h = 0.0f)
int CharacterSheet::button(lua_State* l)
{
	if (assertArgsNum(l, 1, 3)) {
		lua_pushboolean(l, false);
		return 1;
	}

	const char* name = getLuaString(l, 1);
	float w = getLuaFloat(l, 2);
	float h = getLuaFloat(l, 3);

	bool ret = ImGui::Button(name, ImVec2(w, h));
	lua_pushboolean(l, ret);
	return 1;
}

// bool collapsingHeader(string name, int flags = 0)
int CharacterSheet::collapsingHeader(lua_State* l)
{
	if (assertArgsNum(l, 1, 2)) {
		lua_pushboolean(l, false);
		return 1;
	}

	const char* name = getLuaString(l, 1);
	int flags = getLuaInt(l, 2);

	bool ret = ImGui::CollapsingHeader(name, flags);
	lua_pushboolean(l, ret);
	return 1;
}

// void separator()
int CharacterSheet::separator(lua_State* l)
{
	if (assertArgsNum(l, 0, 0)) {
		return 0;
	}
	ImGui::Separator();
	return 0;
}

// void sameLine(float offset = 0.0f, float spacing = -1.0f)
int CharacterSheet::sameLine(lua_State* l)
{
	if (assertArgsNum(l, 0, 2)) {
		return 0;
	}
	float offset = getLuaFloat(l, 1, 0.0f);
	float spacing = getLuaFloat(l, 2, -1.0f);
	ImGui::SameLine(offset, spacing);
	return 0;
}

// void text(string value)
int CharacterSheet::text(lua_State* l)
{
	if (assertArgsNum(l, 1, 1)) {
		return 0;
	}
	const char* name = getLuaString(l, 1);
	
	ImGui::Text(name);

	return 0;
}

// void dummy(float w, float h)
int CharacterSheet::dummy(lua_State* l)
{
	if (assertArgsNum(l, 2, 2)) {
		return 0;
	}
	float w = getLuaFloat(l, 1);
	float h = getLuaFloat(l, 2);
	ImGui::Dummy(ImVec2(w, h));
	return 0;
}

// void indent()
int CharacterSheet::indent(lua_State* l)
{
	if (assertArgsNum(l, 0, 0)) {
		return 0;
	}
	ImGui::Indent();
	return 0;
}

// void unindent()
int CharacterSheet::unindent(lua_State* l)
{
	if (assertArgsNum(l, 0, 0)) {
		return 0;
	}
	ImGui::Unindent();
	return 0;
}

// void beginGroup()
int CharacterSheet::beginGroup(lua_State* l)
{
	if (assertArgsNum(l, 0, 0)) {
		return 0;
	}
	ImGui::BeginGroup();
	return 0;
}

// void endGroup()
int CharacterSheet::endGroup(lua_State* l)
{
	if (assertArgsNum(l, 0, 0)) {
		return 0;
	}
	ImGui::EndGroup();
	return 0;
}

// void beginChild(string name, float w, float h, bool border, int flags)
int CharacterSheet::beginChild(lua_State* l)
{
	if (assertArgsNum(l, 3, 5)) {
		return 0;
	}

	const char* name = getLuaString(l, 1);
	float w = getLuaFloat(l, 2);
	float h = getLuaFloat(l, 3);
	bool border = getLuaBoolean(l, 4);
	int flags = getLuaInt(l, 5);

	ImGui::BeginChild(name, ImVec2(w, h), border, flags);
	return 0;
}

// void endChild()
int CharacterSheet::endChild(lua_State* l)
{
	if (assertArgsNum(l, 0, 0)) {
		return 0;
	}
	ImGui::EndChild();
	return 0;
}

// void setNextItemWidth(float w)
int CharacterSheet::setNextItemWidth(lua_State* l)
{
	if (assertArgsNum(l, 1, 1)) {
		return 0;
	}
	float w = getLuaFloat(l, 1);
	ImGui::SetNextItemWidth(w);
	return 0;
}

// float2 getContentRegion()
int CharacterSheet::getContentRegion(lua_State* l)
{
	if (assertArgsNum(l, 0, 0)) {
		lua_pushinteger(l, 0);
		lua_pushinteger(l, 0);
		return 2;
	}
	ImVec2 size = ImGui::GetContentRegionAvail();
	lua_pushinteger(l, size.x);
	lua_pushinteger(l, size.y);
	return 2;
}

// string textValue(string name)
int CharacterSheet::textValue(lua_State* l)
{
	if (assertArgsNum(l, 1, 1)) {
		lua_pushstring(l, "");
		return 1;
	}

	std::string name = std::string(getLuaString(l, 1));
	std::string& v = currentCharacter->get<std::string>(name, "");
	lua_pushstring(l, v.c_str());
	return 1;
}

// int intValue(string name)
int CharacterSheet::intValue(lua_State* l)
{
	if (assertArgsNum(l, 1, 1)) {
		lua_pushinteger(l, 0);
		return 1;
	}

	std::string name = std::string(getLuaString(l, 1));
	int& v = currentCharacter->get<int>(name, 0);
	lua_pushinteger(l, v);
	return 1;
}

// float floatValue(string name)
int CharacterSheet::floatValue(lua_State* l)
{
	if (assertArgsNum(l, 1, 1)) {
		lua_pushnumber(l, 0.0f);
		return 1;
	}

	std::string name = std::string(getLuaString(l, 1));
	float& v = currentCharacter->get<float>(name, 0.0f);
	lua_pushnumber(l, v);
	return 1;
}

// bool booleanValue(string name)
int CharacterSheet::booleanValue(lua_State* l)
{
	if (assertArgsNum(l, 1, 1)) {
		lua_pushboolean(l, false);
		return 1;
	}
	std::string name = std::string(getLuaString(l, 1));
	bool& v = currentCharacter->get<bool>(name, false);
	lua_pushboolean(l, v);
	return 1;
}

// int vecSize(string name)
int CharacterSheet::vecSize(lua_State* l)
{
	if (assertArgsNum(l, 1, 1)) {
		lua_pushinteger(l, 0);
		return 1;
	}
	const char* name = getLuaString(l, 1);
	if (currentCharacter->vectors.find(name) == currentCharacter->vectors.end()) {
		lua_pushinteger(l, 0);
		return 1;
	}
	int size = currentCharacter->vectors[name].size();
	lua_pushinteger(l, size);
	return 1;
}

// string vecGetText(string name, int index)
int CharacterSheet::vecGetText(lua_State* l)
{
	if (assertArgsNum(l, 2, 2)) {
		lua_pushstring(l, "");
		return 1;
	}
	const char* name = getLuaString(l, 1);
	int index = getLuaInt(l, 2);
	std::string* data = currentCharacter->getV<std::string>(name, index);
	if (data == nullptr) {
		lua_pushstring(l, "");
		return 1;
	}
	lua_pushstring(l, data->c_str());
	return 1;
}

// int vecGetInt(string name, int index)
int CharacterSheet::vecGetInt(lua_State* l)
{
	if (assertArgsNum(l, 2, 2)) {
		lua_pushinteger(l, 0);
		return 1;
	}
	const char* name = getLuaString(l, 1);
	int index = getLuaInt(l, 2);
	int* data = currentCharacter->getV<int>(name, index);
	if (data == nullptr) {
		lua_pushinteger(l, 0);
		return 1;
	}
	lua_pushinteger(l, *data);
	return 1;
}

// float vecGetFloat(string name, int index)
int CharacterSheet::vecGetFloat(lua_State* l)
{
	if (assertArgsNum(l, 2, 2)) {
		lua_pushnumber(l, 0.0f);
		return 1;
	}
	const char* name = getLuaString(l, 1);
	int index = getLuaInt(l, 2);
	float* data = currentCharacter->getV<float>(name, index);
	if (data == nullptr) {
		lua_pushnumber(l, 0.0f);
		return 1;
	}
	lua_pushnumber(l, *data);
	return 1;
}

// bool vecGetBoolean(string name, int index)
int CharacterSheet::vecGetBoolean(lua_State* l)
{
	if (assertArgsNum(l, 2, 2)) {
		lua_pushboolean(l, false);
		return 1;
	}
	const char* name = getLuaString(l, 1);
	int index = getLuaInt(l, 2);
	bool* data = currentCharacter->getV<bool>(name, index);
	if (data == nullptr) {
		lua_pushboolean(l, false);
		return 1;
	}
	lua_pushboolean(l, *data);
	return 1;
}

// void vecSetText(string name, int index, string value)
int CharacterSheet::vecSetText(lua_State* l) 
{
	if (assertArgsNum(l, 3, 3)) {
		return 0;
	}

	const char* name = getLuaString(l, 1);
	int index = getLuaInt(l, 2);
	const char* value = getLuaString(l, 3);

	currentCharacter->vectors[name][index] = std::string(value);
	return 0;
}

// void vecSetInt(string name, int index, int value)
int CharacterSheet::vecSetInt(lua_State* l)
{
	if (assertArgsNum(l, 3, 3)) {
		return 0;
	}

	const char* name = getLuaString(l, 1);
	int index = getLuaInt(l, 2);
	int value = getLuaInt(l, 3);

	currentCharacter->vectors[name][index] = value;
	return 0;
}

// void vecSetFloat(string name, int index, float value)
int CharacterSheet::vecSetFloat(lua_State* l)
{
	if (assertArgsNum(l, 3, 3)) {
		return 0;
	}

	const char* name = getLuaString(l, 1);
	int index = getLuaInt(l, 2);
	float value = getLuaFloat(l, 3);

	currentCharacter->vectors[name][index] = value;
	return 0;
}

// void vecSetBoolean(string name, int index, bool value)
int CharacterSheet::vecSetBoolean(lua_State* l)
{
	if (assertArgsNum(l, 3, 3)) {
		return 0;
	}

	const char* name = getLuaString(l, 1);
	int index = getLuaInt(l, 2);
	bool value = getLuaBoolean(l, 3);

	currentCharacter->vectors[name][index] = value;
	return 0;
}

// void vecAddText(string name, string value)
int CharacterSheet::vecAddText(lua_State* l)
{
	if (assertArgsNum(l, 2, 2)) {
		return 0;
	}
	const char* name = getLuaString(l, 1);
	const char* value = getLuaString(l, 2);
	currentCharacter->addV<std::string>(name, value);
	return 0;
}

// void vecAddInt(string name, int value)
int CharacterSheet::vecAddInt(lua_State* l)
{
	if (assertArgsNum(l, 2, 2)) {
		return 0;
	}
	const char* name = getLuaString(l, 1);
	int value = getLuaInt(l, 2);
	currentCharacter->addV<int>(name, value);
	return 0;
}

// void vecAddFloat(string name, float value)
int CharacterSheet::vecAddFloat(lua_State* l)
{
	if (assertArgsNum(l, 2, 2)) {
		return 0;
	}
	const char* name = getLuaString(l, 1);
	float value = getLuaFloat(l, 2);
	currentCharacter->addV<float>(name, value);
	return 0;
}

// void vecAddBoolean(string name, bool value)
int CharacterSheet::vecAddBoolean(lua_State* l)
{
	if (assertArgsNum(l, 2, 2)) {
		return 0;
	}
	const char* name = getLuaString(l, 1);
	bool value = getLuaBoolean(l, 2);
	currentCharacter->addV<bool>(name, value);
	return 0;
}

// void vecErase(string name, int index)
int CharacterSheet::vecErase(lua_State* l)
{
	if (assertArgsNum(l, 2, 2)) {
		return 0;
	}
	const char* name = getLuaString(l, 1);
	int index = getLuaInt(l, 2);
	currentCharacter->eraseV(name, index);
	return 0;
}

// bool inputVecText(string vecName, int index, int flags = 0)
int CharacterSheet::inputVecText(lua_State* l)
{
	if (assertArgsNum(l, 2, 3)) {
		lua_pushboolean(l, false);
		return 1;
	}

	const char* name = getLuaString(l, 1);
	int index = getLuaInt(l, 2);
	int flags = getLuaInt(l, 3);

	std::string n = std::string(name) + "##" + std::string(name) + "_vec_" + std::to_string(index);
	std::string* v = currentCharacter->getV<std::string>(name, index);
	bool ret = ImGui::InputText(n.c_str(), v, flags);
	lua_pushboolean(l, ret);
	return 1;
}

// bool inputVecTextMultiline(string vecName, int index, float w = 0.0f, float h = 0.0f, int flags = 0)
int CharacterSheet::inputVecTextMultiline(lua_State* l)
{
	if (assertArgsNum(l, 2, 5)) {
		lua_pushboolean(l, false);
		return 1;
	}

	const char* name = getLuaString(l, 1);
	int index = getLuaInt(l, 2);
	float w = getLuaFloat(l, 3);
	float h = getLuaFloat(l, 4);
	int flags = getLuaInt(l, 5);

	std::string n = std::string(name) + "##" + std::string(name) + "_vec_" + std::to_string(index);
	std::string* v = currentCharacter->getV<std::string>(name, index);
	bool ret = ImGui::InputTextMultiline(n.c_str(), v, ImVec2(w, h), flags);
	lua_pushboolean(l, ret);
	return 1;
}

// bool inputVecInt(string vecName, int index, float speed = 1.0f, int min = 0, int max = 0, string format = "%d", int flags = 0)
int CharacterSheet::inputVecInt(lua_State* l)
{
	if (assertArgsNum(l, 2, 7)) {
		lua_pushboolean(l, false);
		return 1;
	}
	const char* name = getLuaString(l, 1);
	int index = getLuaInt(l, 2);
	float speed = getLuaFloat(l, 3, 1.0f);
	int min = getLuaInt(l, 4, 0);
	int max = getLuaInt(l, 5, 0);
	const char* format = getLuaString(l, 6, "%d");
	int flags = getLuaInt(l, 7, 0);

	std::string n = std::string(name) + "##" + std::string(name) + "_vec_" + std::to_string(index);
	int* v = currentCharacter->getV<int>(name, index);
	bool ret = ImGui::DragInt(n.c_str(), v, speed, min, max, format, flags);
	lua_pushboolean(l, ret);
	return 1;
}

// bool inputVecFloat(string vecName, int index, float speed = 1.0f, float min = 0, float max = 0, string format = "%d", int flags = 0)
int CharacterSheet::inputVecFloat(lua_State* l)
{
	if (assertArgsNum(l, 2, 7)) {
		lua_pushboolean(l, false);
		return 1;
	}
	const char* name = getLuaString(l, 1);
	int index = getLuaInt(l, 2);
	float speed = getLuaFloat(l, 3);
	float min = getLuaInt(l, 4);
	float max = getLuaInt(l, 5);
	const char* format = getLuaString(l, 6);
	int flags = getLuaInt(l, 7);

	std::string n = std::string(name) + "##" + std::string(name) + "_vec_" + std::to_string(index);
	float* v = currentCharacter->getV<float>(name, index);
	bool ret = ImGui::DragFloat(n.c_str(), v, speed, min, max, format, flags);
	lua_pushboolean(l, ret);
	return 1;
}

// bool inputVecBoolean(string vecName, int index)
int CharacterSheet::inputVecBoolean(lua_State* l)
{
	if (assertArgsNum(l, 2, 2)) {
		lua_pushboolean(l, false);
		return 1;
	}
	const char* name = getLuaString(l, 1);
	int index = getLuaInt(l, 2);

	std::string n = std::string(name) + "##" + std::string(name) + "_vec_" + std::to_string(index);
	bool* v = currentCharacter->getV<bool>(name, index);
	bool ret = ImGui::Checkbox(n.c_str(), v);
	lua_pushboolean(l, ret);
	return 1;
}

// bool vecCombo(string name, int index, int argCount, ...string values)
int CharacterSheet::vecCombo(lua_State* l)
{
	int n = lua_gettop(l);
	if (n < 4) {
		std::cout << "wrong args count" << std::endl;
		lua_pushboolean(l, false);
		return 1;
	}

	const char* name = getLuaString(l, 1);
	int index = getLuaInt(l, 2);
	int count = getLuaInt(l, 3);

	std::vector<const char*> values;
	for (int i = 0; i < count; ++i) {
		values.push_back(getLuaString(l, i + 4));
	}

	std::string fullName = std::string(name) + "##" + std::string(name) + "_vec_" + std::to_string(index);
	int* v = currentCharacter->getV<int>(name, index);
	bool ret = ImGui::Combo(fullName.c_str(), v, values.data(), count);
	lua_pushboolean(l, ret);
	return 1;
}
