#pragma once
#include "Window.h"
#include "Player.h"
#include "LuaReader.h"

class CharacterSheet : public Window {
public:
	CharacterSheet(std::string name, int uid);

	virtual void DrawContent() override;
	virtual void ProcessIO(ImGuiIO& io) {}

	void SetPlayer(std::string playerName);
private:

	std::shared_ptr<Player> ch;

	friend class SettingsBrowser;
	friend class Settings;
	static void SetConfigFile(std::string filePath);
	static std::string luaConfig;

	static LuaReader lua;

	static std::shared_ptr<Player> currentCharacter;

	// lua helpers
	static bool assertArgsNum(lua_State* l, int min, int max);
	static const char* getLuaString(lua_State* l, int index, const char* d = "");
	static int getLuaInt(lua_State* l, int index, int d = 0);
	static float getLuaFloat(lua_State* l, int index, float d = 0.0f);
	static bool getLuaBoolean(lua_State* l, int index, bool d = false);

	// lua functions
	static int inputText(lua_State* l);
	static int inputTextMultiline(lua_State* l);
	static int inputInt(lua_State* l);
	static int inputFloat(lua_State* l);

	static int displayText(lua_State* l);
	static int displayTextMultiline(lua_State* l);
	static int displayInt(lua_State* l);
	static int displayFloat(lua_State* l);

	static int setText(lua_State* l);
	static int setInt(lua_State* l);
	static int setFloat(lua_State* l);
	static int setBoolean(lua_State* l);

	static int getText(lua_State* l);
	static int getInt(lua_State* l);
	static int getFloat(lua_State* l);
	static int getBoolean(lua_State* l);

	static int checkbox(lua_State* l);
	static int combo(lua_State* l);
	static int selectable(lua_State* l);
	static int button(lua_State* l);
	static int collapsingHeader(lua_State* l);

	static int separator(lua_State* l);
	static int sameLine(lua_State* l);
	static int text(lua_State* l);
	static int dummy(lua_State* l);
	static int indent(lua_State* l);
	static int unindent(lua_State* l);
	static int beginGroup(lua_State* l);
	static int endGroup(lua_State* l);
	static int beginChild(lua_State* l);
	static int endChild(lua_State* l);
	static int setNextItemWidth(lua_State* l);
	static int getContentRegion(lua_State* l);

	static int textValue(lua_State* l);
	static int intValue(lua_State* l);
	static int floatValue(lua_State* l);
	static int booleanValue(lua_State* l);

	static int vecSize(lua_State* l);
	static int vecGetText(lua_State* l);
	static int vecGetInt(lua_State* l);
	static int vecGetFloat(lua_State* l);
	static int vecGetBoolean(lua_State* l);
	static int vecSetText(lua_State* l);
	static int vecSetInt(lua_State* l);
	static int vecSetFloat(lua_State* l);
	static int vecSetBoolean(lua_State* l);
	static int vecAddText(lua_State* l);
	static int vecAddInt(lua_State* l);
	static int vecAddFloat(lua_State* l);
	static int vecAddBoolean(lua_State* l);
	static int vecErase(lua_State* l);
	static int inputVecText(lua_State* l);
	static int inputVecTextMultiline(lua_State* l);
	static int inputVecInt(lua_State* l);
	static int inputVecFloat(lua_State* l);
	static int inputVecBoolean(lua_State* l);
	static int vecCombo(lua_State* l);
};