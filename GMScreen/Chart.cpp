#include "Chart.h"
#include "ImGUI/imgui_stdlib.h"
#include <optional>

void Chart::AddNode(std::string name, glm::vec2 position)
{
	int id = -1;
	for (auto& i : idPool) {
		if (id < 0 || id > i) id = i;
	}

	nodes.push_back(std::make_shared<Chart::Node>(id, name, position));
	idPool.erase(idPool.find(id));
	if (idPool.empty()) {
		idPool.insert(id + 1);
	}
}

void Chart::RemoveNode(std::shared_ptr<Chart::Node> n)
{
	int id = n->id;
	nodes.erase(std::remove_if(nodes.begin(), nodes.end(), [&](std::shared_ptr<Chart::Node>& node) { return node == n; }));
	idPool.insert(id);
}

void Chart::Display()
{
	ImGui::InputText("Name", &name);

	ImGui::Separator();

	// tags
	auto draw_list = ImGui::GetWindowDrawList();
	std::string tagToRemove;

	float tag_height = 25.0f;
	float tag_remove_button_width = 20.0f;

	float maxWidth = ImGui::GetWindowWidth() * 0.7;
	float currWidth = 0;
	float spacing = 5.0f;
	float padding = 2.0f;

	for (auto& tag : tags) {
		ImVec2 text_size = ImGui::CalcTextSize(tag.c_str());
		ImVec2 size = text_size;
		size.y = tag_height;
		size.x += tag_remove_button_width + 2 * padding;

		if (currWidth > 0) {
			if (currWidth + size.x + spacing > maxWidth) {
				currWidth = 0;
			}
			else {
				ImGui::SameLine(0.0f, spacing);
				currWidth += spacing;
			}
		}

		ImGui::Dummy(ImVec2(0, 0));
		ImVec2 min = ImGui::GetItemRectMin();
		draw_list->AddRectFilled(min, ImVec2(min.x + size.x, min.y + size.y), ImColor(0.4f, 0.4f, 0.4f, 1.0f), 10.0f);
		draw_list->AddText(ImVec2(min.x + padding, min.y + (size.y / 2) - (text_size.y / 2)), ImColor(1.0f, 1.0f, 1.0f, 1.0f), tag.c_str());
		ImGui::SameLine(0.0f, padding);
		ImGui::Dummy(ImVec2(text_size.x, size.y));

		ImGui::SameLine(0.0f, padding);
		if (ImGui::Button(("x##npc_remove_tag_" + tag).c_str(), ImVec2(tag_remove_button_width, tag_height))) {
			tagToRemove = tag;
		}

		currWidth += size.x;
	}
	if (tagToRemove != "") {
		tags.erase(std::find(tags.begin(), tags.end(), tagToRemove));
		tagToRemove = "";
	}

	ImGui::InputText("##new_tag_name", &newTagName);
	ImGui::SameLine();
	if (ImGui::Button("+##add_new_tag") && newTagName != "") {
		if (std::find(std::begin(tags), std::end(tags), newTagName) == std::end(tags)) {
			tags.push_back(newTagName);
		}
		newTagName = "";
	}

	// data
	int toRemove = -1;
	int noteNum = 0;
	for (auto& note : info) {
		std::string buttonId = "-##-" + std::to_string(noteNum);
		if (ImGui::Button(buttonId.c_str())) {
			toRemove = noteNum;
		}
		ImGui::SameLine();
		std::string headerName = note.first + "##" + note.first + std::to_string(noteNum);
		if (ImGui::CollapsingHeader(headerName.c_str())) {
			std::string descName = "##noteDesc" + std::to_string(noteNum);
			ImGui::InputTextMultiline(descName.c_str(), &note.second);
		}
		noteNum++;
	}

	if (toRemove >= 0) {
		noteNum = 0;
		info.erase(info.begin() + noteNum);
	}

	ImGui::Separator();
	ImGui::InputText("##new", &newInfoTitle);
	ImGui::SameLine();
	if (ImGui::Button("+")) {
		if (newInfoTitle != "") {
			info.push_back(std::make_pair(newInfoTitle, std::string("")));
			newInfoTitle = "";
		}
	}
}

void Chart::Node::Draw(ImDrawList* drawList, glm::vec2 posOffset, bool hovered, bool selected)
{
	ImVec2 textSize = ImGui::CalcTextSize(title.c_str());
	size = glm::dvec2(textSize.x + 20, textSize.y + 20);
	size.x = glm::clamp(size.x, minSize.x, maxSize.x);
	size.y = glm::clamp(size.y, minSize.y, maxSize.y);

	glm::vec2 labelPosition = position + posOffset + glm::vec2(10.0f);
	ImColor labelColor = ImColor(0.0f, 0.0f, 0.0f);
	int labelSize = ImGui::GetFontSize();
	std::string labelText = title;

	glm::vec2 pos = position + posOffset;
	ImVec2 a(pos.x, pos.y);
	ImVec2 b(a.x + size.x, a.y + size.y);

	ImColor bgColor = color;
	ImColor borderColor = ImColor(0.0f, 0.0f, 0.0f);

	drawList->AddRectFilled(a, b, bgColor, 10.0f, ImDrawCornerFlags_All);
	drawList->AddRect(a, b, borderColor, 10.0f, ImDrawCornerFlags_All, 5.0f);
	
	std::optional<ImColor> highlight;
	if (selected) {
		highlight = ImColor(0.8f, 0.8f, 0.4f);
	}
	else if (hovered) {
		highlight = ImColor(0.4f, 0.4f, 0.8f);
	}


	if (highlight.has_value()) {
		ImVec2 a1, a2, b1, b2;
		a1 = { a.x - 2.0f, a.y - 2.0f };
		b1 = { b.x + 2.0f, b.y + 2.0f };
		a2 = { a.x + 2.0f, a.y + 2.0f };
		b2 = { b.x - 2.0f, b.y - 2.0f };
		drawList->AddRect(a1, b1, highlight.value(), 10.0f);
		drawList->AddRect(a2, b2, highlight.value(), 10.0f);
	}

	ImVec2 textPos{ labelPosition.x, labelPosition.y };
	drawList->AddText(ImGui::GetFont(), labelSize, textPos, labelColor, labelText.c_str(), (const char*)0, size.x - 5.0f);
}

bool Chart::Node::IsMouseOver(glm::vec2 mousePos) const
{
	return mousePos.x >= position.x
		&& mousePos.x <= position.x + size.x
		&& mousePos.y >= position.y
		&& mousePos.y <= position.y + size.y;
}

void Chart::Node::Display()
{
	std::string s = ":" + std::to_string(id);
	ImGui::InputTextMultiline(("##node_content"+s).c_str(), &title);
	ImGui::Separator();
	int toRemove = -1;
	int noteNum = 0;
	for (auto& note : data) {
		std::string buttonId = "-##-" + std::to_string(noteNum) + s;
		if (ImGui::Button(buttonId.c_str())) {
			toRemove = noteNum;
		}
		ImGui::SameLine();
		std::string headerName = note.first + "##" + note.first + std::to_string(noteNum) + s;
		if (ImGui::CollapsingHeader(headerName.c_str())) {
			std::string descName = "##noteDesc" + std::to_string(noteNum);
			ImGui::InputTextMultiline(descName.c_str(), &note.second);
		}
		noteNum++;
	}

	if (toRemove >= 0) {
		noteNum = 0;
		data.erase(data.begin() + noteNum);
	}

	ImGui::Separator();
	ImGui::InputText(("##new" + s).c_str(), &newDataTitle);
	ImGui::SameLine();
	if (ImGui::Button("+")) {
		if (newDataTitle != "") {
			data.push_back(std::make_pair(newDataTitle, std::string("")));
			newDataTitle = "";
		}
	}
}
