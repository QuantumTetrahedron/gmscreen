#pragma once
#include <string>
#include <cereal/access.hpp>
#include "Properties.h"
#include <glm/glm.hpp>
#include <set>

class Chart : public IDisplayable {
public:
	class Node : public IDisplayable {
	public:
		Node(int id, std::string t, glm::vec2 pos)
			: title(t), position(pos), id(id) {
			size = minSize;
		}

		int id;
		glm::vec2 position;
		glm::dvec2 size;
		std::string title;
		std::vector<std::pair<std::string, std::string>> data;
		ImColor color = ImColor(0.75f, 0.75f, 0.75f, 0.8f);

		void Draw(ImDrawList* drawList, glm::vec2 posOffset, bool hovered, bool selected);
		bool IsMouseOver(glm::vec2 mousePos) const;

		void Display();
	private:
		friend class cereal::access;
		template<class Archive>
		void serialize(Archive& ar) {
			ar(position.x, position.y, size.x, size.y, title, data);
		}
		std::string newDataTitle = "";
		glm::dvec2 minSize = glm::vec2(50, 20);
		glm::dvec2 maxSize = glm::vec2(500, 500);
	};

	Chart(std::string name) : name(name) {
		idPool.insert(0);
	}

	std::string name;
	std::vector<std::string> tags;
	std::vector<std::pair<std::string, std::string>> info;
	std::vector<std::shared_ptr<Node>> nodes;

	void AddNode(std::string name, glm::vec2 position);
	void RemoveNode(std::shared_ptr<Chart::Node> n);
private:

	friend class cereal::access;
	void Display() override;
	Chart()
		: name("") {}
	template<class Archive>
	void serialize(Archive& ar) {
		ar(name, tags, info, nodes, idPool);
	}

	std::string newTagName;
	std::string newInfoTitle;

	std::set<int> idPool;
};