#include "ChartWorkspace.h"
#include "ResourcesManager.h"

const int DELETE_KEY_CODE = 261;

ChartWorkspace::ChartWorkspace(std::string name, int uid)
	: Window(name, uid)
{
	startDocked = true;
	SetWindowFlags(GetWindowFlags() | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoSavedSettings);
	
	for (auto ch : ResourcesManager::charts) {
		if (ch->name == name) {
			chart = ch;
			break;
		}
	}
}

void ChartWorkspace::DrawContent()
{
	hovered = ImGui::IsWindowHovered();

	if (!chart) {
		return;
	}

	auto draw_list = ImGui::GetWindowDrawList();

	ImVec2 bgSize;
	bgSize.x = ImGui::GetWindowContentRegionWidth();
	bgSize.y = glm::max(10.0f, ImGui::GetWindowSize().y - 40);

	const ImVec2 p = ImGui::GetCursorScreenPos();

	// draw nodes

	for (auto& node : chart->nodes) {
		node->Draw(draw_list, workspaceOffset, node == hoveredNode, node == selectedNode);
	}

	// draw connections

	// context menu

    ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 1.0f);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(8.0f, 8.0f));

    if (ImGui::IsWindowHovered() && ImGui::IsMouseReleased(ImGuiMouseButton_Right) && shouldOpenContextMenu) {
        ImGui::OpenPopup("contextMenu");
        shouldOpenContextMenu = false;
    }
    if (ImGui::BeginPopup("contextMenu")) {
        if (ImGui::MenuItem("Add node")) {
			chart->AddNode("", mousePos);
        }
        ImGui::EndPopup();
    }

    ImGui::PopStyleVar(2);

}

void ChartWorkspace::ProcessIO(ImGuiIO& io)
{
	if (!hovered || !chart) {
		state = State::Idle;
		return;
	}
	mousePos = glm::vec2(io.MousePos.x, io.MousePos.y) - workspaceOffset;

	hoveredNode = nullptr;
	for (auto& node : chart->nodes) {
		if (node->IsMouseOver(mousePos)) {
			hoveredNode = node;
			break;
		}
	}

	switch (state) {
	case State::Idle:
		IdleIO(io);
		break;
	case State::ViewDrag:
		ViewDragIO(io);
		break;
	case State::NodeDrag:
		NodeDragIO(io);
		break;
	case State::LineDrag:
		LineDragIO(io);
		break;
	}
}

void ChartWorkspace::BringSelectedToFront()
{
}

void ChartWorkspace::IdleIO(ImGuiIO& io)
{
    if (io.MouseClicked[0]) {
        if (hoveredNode) {
            selectedNode = hoveredNode;
            dragOffset = mousePos - selectedNode->position;
            state = State::NodeDrag;
            
            BringSelectedToFront();
			Properties::Display(selectedNode);
        }
    }

    if (io.MouseClicked[1]) {
		shouldOpenContextMenu = true;
    }

    if (io.MouseClicked[2]) {
        glm::vec2 clickPos = glm::vec2(io.MouseClickedPos[2].x, io.MouseClickedPos[2].y);
        state = State::ViewDrag;
        dragOffset = clickPos - workspaceOffset;
    }

	if (ImGui::IsKeyPressed(ImGuiKey_Delete, false) && selectedNode) {
		chart->RemoveNode(selectedNode);
		selectedNode = nullptr;
	}
    /*static bool press = false;
    if (ImGui::IsKeyDown(ImGuiKey_Delete) && !press && selectedNode) {
        press = true;
    }
    else if (!ImGui::IsKeyDown(ImGuiKey_Delete) && press) {
        press = false;
    }*/
}

void ChartWorkspace::ViewDragIO(ImGuiIO& io)
{
	glm::vec2 mousePos = glm::vec2(io.MousePos.x, io.MousePos.y);
	workspaceOffset = mousePos - dragOffset;

	if (io.MouseReleased[2]) {
		state = State::Idle;
	}
}

void ChartWorkspace::NodeDragIO(ImGuiIO& io)
{
	glm::vec2 mousePos = glm::vec2(io.MousePos.x, io.MousePos.y) - workspaceOffset;
	selectedNode->position = mousePos - dragOffset;

	if (io.MouseReleased[0]) {
		state = State::Idle;
	}
}

void ChartWorkspace::LineDragIO(ImGuiIO& io)
{
}
