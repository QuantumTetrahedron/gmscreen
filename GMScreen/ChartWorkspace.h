#pragma once
#include "Window.h"
#include "glm/glm.hpp"
#include "Chart.h"

class ChartWorkspace : public Window {
public:
	ChartWorkspace(std::string name, int uid);

	virtual void DrawContent() override;
	virtual void ProcessIO(ImGuiIO& io);

private:
	bool shouldOpenContextMenu = false;

	void BringSelectedToFront();

	enum class State {
		Idle, ViewDrag, NodeDrag, LineDrag
	};

	void IdleIO(ImGuiIO& io);
	void ViewDragIO(ImGuiIO& io);
	void NodeDragIO(ImGuiIO& io);
	void LineDragIO(ImGuiIO& io);

	std::shared_ptr<Chart> chart;

	glm::vec2 workspaceOffset = glm::vec2(0.0f);
	glm::vec2 dragOffset = glm::vec2(0.0f);
	glm::vec2 mousePos = glm::vec2(0.0f);
	bool hovered = false;
	float scale = 1.0f;

	State state = State::Idle;

	std::shared_ptr<Chart::Node> hoveredNode;
	std::shared_ptr<Chart::Node> selectedNode;
};