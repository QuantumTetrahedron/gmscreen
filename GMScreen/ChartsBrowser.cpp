#include "ChartsBrowser.h"
#include "ImGUI/imgui_stdlib.h"
#include "ResourcesManager.h"
#include "StringSearch.h"
#include "WindowManager.h"
#include "ChartWorkspace.h"

ChartsBrowser::ChartsBrowser(std::string name, int uid)
	: Window(name, uid)
{
}

void ChartsBrowser::DrawContent()
{
    ImGui::InputText("##charts_searchbar", &filter);

    ImVec2 size = ImGui::GetWindowSize();
    size.y = 0;
    size.x -= 20;

    if (ImGui::BeginListBox("##charts_list", size)) {
        int i = 0;
        for (std::shared_ptr<Chart>& chart : ResourcesManager::charts) {
            bool found = filter == "";
            std::string searchTagUsed = "";
            if (!found) {
                found = find(chart->name, filter);
            }

            if (!found) {
                for (std::string& tag : chart->tags) {
                    found = find(tag, filter);
                    if (found) {
                        searchTagUsed = tag;
                        break;
                    }
                }
            }

            if (!found) {
                continue;
            }

            std::string name = chart->name;
            if (searchTagUsed != "") {
                name = "[" + searchTagUsed + "] " + name;
            }
            if (ImGui::Selectable(name.c_str(), selected == i)) {
                selected = i;
                Properties::Display(chart);
            }

            if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left)) {
                WindowManager::OpenWindow<ChartWorkspace>(chart->name);
            }

            ++i;
        }
        ImGui::EndListBox();
    }

    ImGui::InputText("##new_name", &newName);
    if (ImGui::Button("Add##add_chart") && newName != "") {
        ResourcesManager::charts.push_back(std::make_shared<Chart>(newName));
        newName = "";
    }
}

