#pragma once
#include "Window.h"

class ChartsBrowser : public Window {
public:
	ChartsBrowser(std::string name, int uid);
	virtual void DrawContent() override;
	virtual void ProcessIO(ImGuiIO& io) {};
private:
	int selected = -1;
	std::string filter;
	std::string newName;
};