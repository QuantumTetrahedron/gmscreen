#include "CommandPool.h"

VkCommandPool CommandPool::commandPool;

void CommandPool::create()
{
	VkCommandPoolCreateInfo poolInfo{};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = VulkanLoader::GetQueueFamilyIndex();
	poolInfo.flags = 0;

	vkCreateCommandPool(VulkanLoader::GetDevice(), &poolInfo, nullptr, &commandPool);
}

void CommandPool::cleanup()
{
	vkDestroyCommandPool(VulkanLoader::GetDevice(), commandPool, nullptr);
}

void CommandPool::Allocate(uint32_t count, VkCommandBuffer* commands)
{
	VkCommandBufferAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = count;
	vkAllocateCommandBuffers(VulkanLoader::GetDevice(), &allocInfo, commands);
}

void CommandPool::Free(uint32_t count, VkCommandBuffer* commands)
{
	vkFreeCommandBuffers(VulkanLoader::GetDevice(), commandPool, count, commands);
}

VkCommandBuffer CommandPool::beginSingleTimeCommands()
{
	VkCommandBuffer command;
	Allocate(1, &command);

	VkCommandBufferBeginInfo beginInfo{};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	vkBeginCommandBuffer(command, &beginInfo);

	return command;
}

void CommandPool::endSingleTimeCommands(VkCommandBuffer command)
{

	vkEndCommandBuffer(command);

	VkSubmitInfo submitInfo{};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &command;

	vkQueueSubmit(VulkanLoader::GetQueue(), 1, &submitInfo, VK_NULL_HANDLE);
	vkQueueWaitIdle(VulkanLoader::GetQueue());

	Free(1, &command);
}
