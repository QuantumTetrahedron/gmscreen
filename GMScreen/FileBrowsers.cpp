#include "FileBrowsers.h"

ImGui::FileBrowser FileBrowsers::SaveBrowser = ImGui::FileBrowser(ImGuiFileBrowserFlags_EnterNewFilename | ImGuiFileBrowserFlags_CreateNewDir);
ImGui::FileBrowser FileBrowsers::OpenBrowser;

std::filesystem::path FileBrowsers::getLocalPath(std::filesystem::path globalPath)
{
	return globalPath.lexically_relative(std::filesystem::current_path());
}
