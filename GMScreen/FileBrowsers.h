#pragma once
#include "ImGUI/imgui.h"
#include "imfilebrowser.h"

class FileBrowsers {
public:
	static ImGui::FileBrowser SaveBrowser;
	static ImGui::FileBrowser OpenBrowser;

	static std::filesystem::path getLocalPath(std::filesystem::path globalPath);
};