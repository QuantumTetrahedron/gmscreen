#pragma once

class ImGuiStyleHelper
{
public:
	static void SetStyle_charcoal();
	static void SetStyle_corporateGrey();
	static void SetStyle_blackAndGold();
	static void SetStyle_deepDark();
	static void SetStyle_darkRed();
};


