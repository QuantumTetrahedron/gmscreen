#pragma once
#include "Buffer.h"

class Image
{
public:
	void create(uint32_t w, uint32_t h, VkFormat f, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImageAspectFlags aspect = VK_IMAGE_ASPECT_COLOR_BIT);
	void cleanup();

	void transitionLayout(VkImageLayout newLayout);
	void transitionLayout(VkCommandBuffer command, VkImageLayout newLayout);
	void copyBuffer(Buffer& buffer);
	void copyTo(VkCommandBuffer command, VkImage target, VkImageLayout targetLayout, bool blit);

	VkImageView createImageView();

	uint32_t width, height;
	VkFormat format;
	VkImage image;
private:
	VkImageLayout layout;
	VkImageAspectFlags aspect;

	VkDeviceMemory memory;
};
