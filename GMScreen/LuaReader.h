#pragma once
#include <iostream>
#include "ImGUI/imgui.h"
#include <fstream>

extern "C" {
	#include "lua.h"
	#include "lualib.h"
	#include "lauxlib.h"
}

class LuaReader {
public:
	LuaReader() {
		L = luaL_newstate();
		luaL_openlibs(L);
	}

	void Register(const char* name, lua_CFunction fn) {
		lua_register(L, name, fn);
	}

	void Read(const char* filename) {
		
		if (luaL_dofile(L, filename)) {
			std::cout << "LUA ERROR: " << lua_tostring(L, -1) << std::endl;
		}
	}

	~LuaReader() {
		lua_close(L);
	}

private:
	lua_State* L;
};