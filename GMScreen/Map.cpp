#include "Map.h"
#include "ImGUI/imgui_stdlib.h"
#include "FileBrowsers.h"
#include "ResourcesManager.h"

Map::Map(std::string name, MapData data, glm::vec2 size)
	: name(name), data(data), size(size) {}

std::string Map::GetName()
{
	return name;
}

MapData Map::GetData()
{
	return data; 
}

glm::vec2 Map::GetSize()
{
	return size;
}

void Map::AddField(glm::ivec2 coords, std::shared_ptr<Field> fieldData)
{
	if (fields.find(coords.x) != fields.end()) {
		auto& row = fields.at(coords.x);
		if (row.find(coords.y) != row.end()) {
			std::cout << "Map field already exists" << std::endl;
			return;
		}
		else {
			row[coords.y] = fieldData;
		}
	}
	else {
		fields[coords.x] = {};
		fields[coords.x][coords.y] = fieldData;
	}
}

void Map::RemoveField(glm::ivec2 coords)
{
	fields.at(coords.x).erase(coords.y);
}

std::shared_ptr<Field> Map::GetField(glm::ivec2 coords)
{
	if (fields.find(coords.x) != fields.end()) {
		auto& row = fields.at(coords.x);
		if (row.find(coords.y) != row.end()) {
			return row.at(coords.y);
		}
		return nullptr;
	}
	return nullptr;
}

std::string Map::GetSelectedViewTextureName()
{
	return data.views[selectedView].second;
}

void Map::Display()
{
	ImGui::Text(name.c_str());
	if (ImGui::DragFloat("Cell size", &data.cellSize)) {
		data.cellSize = glm::clamp(data.cellSize, 10.0f, 200.0f);
	}
	ImGui::DragFloat2("Offset", &data.cellOffset[0]);
	ImGui::ColorEdit4("Lines color", &data.linesColor[0]);
	ImGui::ColorEdit4("Selection color", &data.selectionColor[0]);

	ImGui::Separator();

	std::vector<int> viewsToRemove;
	if (ImGui::CollapsingHeader("Views")) {
		int i = 0;
		ImGui::BeginListBox("##views");
		for (auto& p : data.views)
		{
			if (i > 0) {
				std::string buttonId = std::string("-##-") + std::to_string(i);
				if (ImGui::Button("-##-")) {
					viewsToRemove.push_back(i);
				}
				ImGui::SameLine();
			}
			if (ImGui::Selectable(p.first.c_str(), i == selectedView)) {
				selectedView = i;
			}
			i++;
		}
		ImGui::EndListBox();

		ImGui::InputText("New view name", &newViewName);
		if (newViewPath == "") {
			std::string tooltip = "choose a file...";
			ImGui::InputText("##New view path tooltip", &tooltip, ImGuiInputTextFlags_ReadOnly);
		}
		else {
			ImGui::InputText("##New view path", &newViewPath, ImGuiInputTextFlags_ReadOnly);
		}
		ImGui::SameLine();


		if (ImGui::Button("Select")) {
			FileBrowsers::OpenBrowser.SetTypeFilters({ ".png", ".jpg" });
			FileBrowsers::OpenBrowser.Open();
		}
		FileBrowsers::OpenBrowser.Display();
		if (FileBrowsers::OpenBrowser.HasSelected())
		{
			newViewPath = FileBrowsers::getLocalPath(FileBrowsers::OpenBrowser.GetSelected()).string();
			FileBrowsers::OpenBrowser.ClearSelected();
			FileBrowsers::OpenBrowser.Close();
			if (!ResourcesManager::HasTexture(newViewPath)) {
				newViewTexture = ResourcesManager::CreateTexture(newViewPath);
			}
		}


		if (ImGui::Button("Add") && newViewPath != "" && newViewName != "") {
			data.views.push_back(std::make_pair(newViewName, newViewPath));

			if (!ResourcesManager::HasTexture(newViewPath) && newViewTexture) {
				ResourcesManager::SaveTexture(newViewPath, newViewTexture);
			}

			newViewPath = "";
			newViewName = "";
			newViewTexture = nullptr;
		}
	}

	if (!viewsToRemove.empty()) {
		int viewNum;
		for (int r : viewsToRemove) {
			viewNum = 0;
			data.views.erase(std::remove_if(data.views.begin(), data.views.end(), [&](auto& v) {
				bool remove = viewNum == r;
				viewNum++;
				return remove;
				}), data.views.end());
		}

		if (selectedView >= data.views.size()) {
			selectedView = data.views.size() - 1;
		}
	}
}

void Field::Display()
{
	std::vector<int> toRemove;
	int noteNum = 0;
	for (auto& note : notes) {
		std::string buttonId = "-##-" + std::to_string(noteNum);
		if(ImGui::Button(buttonId.c_str())) {
			toRemove.push_back(noteNum);
		}
		ImGui::SameLine();
		std::string headerName = note.first + "##" + note.first + std::to_string(noteNum);
		if (ImGui::CollapsingHeader(headerName.c_str())) {
			std::string descName = "##noteDesc" + std::to_string(noteNum);
			ImGui::InputTextMultiline(descName.c_str(), &note.second);
		}
		noteNum++;
	}

	for (int r : toRemove) {
		noteNum = 0;
		notes.erase(std::remove_if(notes.begin(), notes.end(), [&](auto& n) {
			bool remove = noteNum == r;
			noteNum++;
			return remove;
			}), notes.end());
	}

	ImGui::Separator();
	ImGui::InputText("##new", &newNoteTitleBuffer);
	ImGui::SameLine();
	if (ImGui::Button("+")) {
		if (newNoteTitleBuffer != "") {
			notes.push_back(std::make_pair(newNoteTitleBuffer, std::string("")));
			newNoteTitleBuffer = "";
		}
	}
}