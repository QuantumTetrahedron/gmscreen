#pragma once
#include <string>
#include <glm/glm.hpp>
#include <map>
#include "Properties.h"
#include "Texture.h"

#include <cereal/access.hpp>

struct MapData {
public:
	std::vector<std::pair<std::string, std::string>> views;
	glm::vec4 linesColor;
	glm::vec4 selectionColor;
	float cellSize;
	glm::vec2 cellOffset;

private:

	friend class cereal::access;
	template <class Archive>
	void serialize(Archive& ar) {
		ar(views,
			linesColor.r, linesColor.g, linesColor.b, linesColor.a,
			selectionColor.r, selectionColor.g, selectionColor.b, selectionColor.a,
			cellSize,
			cellOffset.x, cellOffset.y);
	}
};

class Field : public IDisplayable {
public:
	std::vector<std::pair<std::string, std::string>> notes;

	std::string newNoteTitleBuffer;
private:
	friend class cereal::access;
	template <class Archive>
	void serialize(Archive& ar){
		ar(notes);
	}

	void Display() override;
};

class Map : public IDisplayable {
public:
	Map(std::string name, MapData data, glm::vec2 size);

	std::string GetName();
	MapData GetData();
	glm::vec2 GetSize();

	void AddField(glm::ivec2 coords, std::shared_ptr<Field> fieldData);
	void RemoveField(glm::ivec2 coords);
	std::shared_ptr<Field> GetField(glm::ivec2 coords);

	std::string GetSelectedViewTextureName();
private:
	std::string name;
	MapData data;
	std::map<int, std::map<int, std::shared_ptr<Field>>> fields;
	glm::vec2 size;
	void Display() override;

	struct Area {
		std::string name;
		std::vector<glm::ivec2> fields;
		bool visible;

		void ToggleField(glm::ivec2 pos) {
			auto it = std::find(fields.begin(), fields.end(), pos);
			if (it != fields.end()) {
				fields.erase(it);
			}
			else {
				fields.push_back(pos);
			}
		}

		template <class Archive>
		void save(Archive& ar) const {
			std::vector<std::pair<int, int>> fs;
			for (auto& field : fields) {
				fs.push_back(std::make_pair(field.x, field.y));
			}
			ar(name, fs, visible);
		}

		template <class Archive>
		void load(Archive& ar) {
			std::vector<std::pair<int, int>> fs;
			ar(name, fs, visible);
			fields.clear();
			for (auto& p : fs) {
				fields.push_back(glm::ivec2(p.first, p.second));
			}
		}
	};
	std::vector<Area> areas;

	int selectedView = 0;
	std::string newViewName;
	std::string newViewPath;
	std::shared_ptr<Texture> newViewTexture;

	Map() {}
	friend class MapWorkspace;
	friend class cereal::access;

	template <class Archive>
	void save(Archive& ar) const {
		std::vector<std::tuple<int, int, std::shared_ptr<Field>>> savedFields;
		for (auto& row : fields) {
			int x = row.first;
			for (auto& f : row.second) {
				int y = f.first;
				if (!f.second->notes.empty()) {
					savedFields.push_back(std::make_tuple(x, y, f.second));
				}
			}
		}
		ar(name, data, savedFields, size.x, size.y, areas);
	}

	template <class Archive>
	void load(Archive& ar) {
		std::vector<std::tuple<int, int, std::shared_ptr<Field>>> loadedFields;
		ar(name, data, loadedFields, size.x, size.y, areas);

		fields = {};

		for (auto& t : loadedFields) {
			int x = std::get<0>(t);
			int y = std::get<1>(t);
			std::shared_ptr<Field> f = std::get<2>(t);
			fields[x][y] = f;
		}
	}
};