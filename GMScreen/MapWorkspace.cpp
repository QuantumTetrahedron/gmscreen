#include "MapWorkspace.h"
#include "ResourcesManager.h"
#include <iostream>
#include "WindowManager.h"
#include "ImGUI/imgui_stdlib.h"

MapWorkspace::MapWorkspace(std::string name, int uid)
	: Window(name, uid)
{
	startDocked = true;
	SetWindowFlags(GetWindowFlags() | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoSavedSettings);
	SetMap(name);
}

void MapWorkspace::DrawContent()
{
	hovered = ImGui::IsWindowHovered();

	if (!map) {
		return;
	}

	UpdateView();

	auto draw_list = ImGui::GetWindowDrawList();

	ImVec2 bgSize;
	bgSize.x = ImGui::GetWindowContentRegionWidth();
	bgSize.y = glm::max(10.0f, ImGui::GetWindowSize().y - 40);

	glm::vec2 imageSize = map->GetSize() * scale;
	const ImVec2 p = ImGui::GetCursorScreenPos();
	mapScreenPoint = glm::vec2(p.x, p.y);

	float uv_x = bgSize.x / imageSize.x;
	float uv_y = bgSize.y / imageSize.y;

	glm::vec2 uv_offset;
	uv_offset.x = -mapOffset.x / imageSize.x;
	uv_offset.y = -mapOffset.y / imageSize.y;

	ImVec2 uv1(uv_offset.x, uv_offset.y);
	ImVec2 uv2(uv_x + uv_offset.x, uv_y + uv_offset.y);

	ImGui::Image(mapTexture->getDescriptor(), bgSize, uv1, uv2, ImVec4(1, 1, 1, 1), ImVec4(0.5f, 0.5f, 0.5f, 1.0f));

	MapData data = map->GetData();
	glm::vec4 c = data.linesColor;
	ImColor color(c.r, c.g, c.b, c.a);

	glm::vec2 cellOffset = map->GetData().cellOffset * scale;
	glm::vec2 minPos = mapOffset + cellOffset;
	glm::vec2 texEnd = mapOffset + map->GetSize() * scale;
	glm::vec2 maxPos = minPos;
	float cellSize = data.cellSize * scale;
	for (float dx = minPos.x; dx <= texEnd.x; dx += cellSize) {
		maxPos.x = dx;
	}
	for (float dy = minPos.y; dy <= texEnd.y; dy += cellSize) {
		maxPos.y = dy;
	}

	int totalColumns = 0;
	int totalRows = 0;

	// horizontal lines
	for (float yPos = minPos.y; yPos < maxPos.y; yPos += cellSize) {
		ImVec2 p1(p.x + minPos.x, p.y + yPos), p2(p.x + maxPos.x, p.y + yPos);
		draw_list->AddLine(p1, p2, color, 2.0f);
		totalRows++;
	}
	draw_list->AddLine(ImVec2(p.x + minPos.x, p.y + maxPos.y), ImVec2(p.x + maxPos.x, p.y + maxPos.y), color, 2.0f);

	// vertical lines
	for (float xPos = minPos.x; xPos < maxPos.x; xPos += cellSize) {
		ImVec2 p1(p.x + xPos, p.y + minPos.y), p2(p.x + xPos, p.y + maxPos.y);
		draw_list->AddLine(p1, p2, color, 2.0f);
		totalColumns++;
	}
	draw_list->AddLine(ImVec2(p.x + maxPos.x, p.y + minPos.y), ImVec2(p.x + maxPos.x, p.y + maxPos.y), color, 2.0f);

	// hovered tile
	tileHovered = false;
	glm::vec4 sc = data.selectionColor;
	ImColor selColor(sc.r, sc.g, sc.b, sc.a);
	glm::vec2 squarePos = glm::vec2(hoveredTileCoords) * cellSize + cellOffset + mapOffset;
	if (hoveredTileCoords.x >= 0 && hoveredTileCoords.x < totalColumns && hoveredTileCoords.y >= 0 && hoveredTileCoords.y < totalRows) {
		draw_list->AddRectFilled(ImVec2(p.x + squarePos.x, p.y + squarePos.y), ImVec2(p.x + squarePos.x + cellSize, p.y + squarePos.y + cellSize), selColor);
		tileHovered = true;
	}

	// selected tile
	glm::vec2 selectedPos = glm::vec2(selectedFieldCoords) * cellSize + cellOffset + mapOffset;
	if (selectedFieldCoords.x >= 0 && selectedFieldCoords.x < totalColumns && selectedFieldCoords.y >= 0 && selectedFieldCoords.y < totalRows) {
		draw_list->AddRect(ImVec2(p.x + selectedPos.x + 4, p.y + selectedPos.y + 4), ImVec2(p.x + selectedPos.x + cellSize - 4, p.y + selectedPos.y + cellSize - 4), 
			ImColor(sc.r, sc.g, sc.b, 1.0f), 0.0f, 0, 3.0f);
	}

	// filled field markers
	for (std::pair<const int, std::map<int, std::shared_ptr<Field>>>& row : map->fields) {
		int x = row.first;
		for (std::pair<const int, std::shared_ptr<Field>> field : row.second) {
			int y = field.first;
			if (x == selectedFieldCoords.x && y == selectedFieldCoords.y) {
				continue;
			}

			glm::vec2 pos = glm::vec2(x, y) * cellSize + cellOffset + mapOffset;
			float d = cellSize * 0.2f;
			draw_list->AddTriangleFilled(
				ImVec2(p.x + pos.x, p.y + pos.y),
				ImVec2(p.x + pos.x + d, p.y + pos.y),
				ImVec2(p.x + pos.x, p.y + pos.y + d),
				ImColor(sc.r, sc.g, sc.b, 1.0f)
			);
		}
	}

	// areas
	ImColor selectedAreaColor = ImColor(sc.r, sc.g, sc.b, 1.0f);
	ImColor areaColor = ImColor(c.r, c.g, c.b, 1.0f);
	if (selectedTool == 1) {
		selectedAreaColor = ImColor(sc.r, sc.g, sc.b, 0.6f);
		areaColor = ImColor(c.r, c.g, c.b, 0.3f);
	}

	int i = 0;
	for (Map::Area& area : map->areas) {
		if (!area.visible) {
			++i;
			continue;
		}

		for (glm::ivec2 coords : area.fields) {
			ImColor ac = selectedArea == i ? selectedAreaColor : areaColor;
			glm::vec2 pos = glm::vec2(coords) * cellSize + cellOffset + mapOffset;
			if (coords.x >= 0 && coords.x < totalColumns && coords.y >= 0 && coords.y < totalRows) {
				draw_list->AddRectFilled(ImVec2(p.x + pos.x, p.y + pos.y), ImVec2(p.x + pos.x + cellSize, p.y + pos.y + cellSize), ac);
			}
		}
		++i;
	}

	DrawOverlay(p);
}

void MapWorkspace::ProcessIO(ImGuiIO& io)
{
	if (!hovered) {
		state = State::Idle;
		return;
	}

	mousePos = glm::vec2(io.MousePos.x, io.MousePos.y) - mapOffset;
	glm::vec2 point = (glm::vec2(io.MousePos.x, io.MousePos.y) - mapOffset - mapScreenPoint) / scale;
	
	point -= map->GetData().cellOffset;
	float cellSize = map->GetData().cellSize;

	point.x = glm::floor(point.x / cellSize);
	point.y = glm::floor(point.y / cellSize);

	hoveredTileCoords = glm::ivec2(point.x, point.y);

	if (state == State::Idle) {
		if (io.MouseReleased[0]) {
			if (tileHovered) {
				if (selectedTool == 0) {
					SelectField(hoveredTileCoords);
				}
				else if (selectedTool == 1 && selectedArea >= 0 && selectedArea < map->areas.size()) {
					map->areas[selectedArea].ToggleField(hoveredTileCoords);
				}
			}
		}

		if (io.MouseClicked[1]) {
			glm::vec2 clickPos = glm::vec2(io.MouseClickedPos[1].x, io.MouseClickedPos[1].y);
			state = State::Drag;
			dragOffset = clickPos - mapOffset;
		}

		if (io.MouseWheel != 0) {
			float scaleDelta = io.MouseWheel * 0.1;
			float newScale = scale;
			if (scaleDelta > 0) {
				newScale = scale * (1 + scaleDelta);
			}
			else {
				newScale = scale / (1 - scaleDelta);
			}

			float ds = 0.0f;
			if (newScale >= 0.2f && newScale <= 5.0f) {
				ds = newScale / scale - 1.0f;
				scale = newScale;
			}

			glm::vec2 cursorPos = glm::vec2(io.MousePos.x, io.MousePos.y) - mapScreenPoint;
			mapOffset += (-cursorPos + mapOffset) * ds;
		}
	}
	else if (state == State::Drag) {
		glm::vec2 mousePos = glm::vec2(io.MousePos.x, io.MousePos.y);
		mapOffset = mousePos - dragOffset;

		if (io.MouseReleased[1]) {
			state = State::Idle;
		}
	}
}

void MapWorkspace::Cleanup()
{
	vkDeviceWaitIdle(VulkanLoader::GetDevice());
	map = nullptr;
	mapTexture = nullptr;
}

void MapWorkspace::SetMap(std::string mapName)
{
	map = ResourcesManager::GetMap(mapName);
	UpdateView();
}

void MapWorkspace::OnFileLoad()
{
	Cleanup();
	WindowManager::CloseWindow(shared_from_this());
}

void MapWorkspace::UpdateView()
{
	std::string activeViewName = map->GetSelectedViewTextureName();
	if (shownViewTextureName != activeViewName) {
		shownViewTextureName = activeViewName;
		mapTexture = ResourcesManager::GetTexture(shownViewTextureName);
	}
}

void MapWorkspace::SelectField(glm::ivec2 coords)
{
	if (selectedFieldCoords == coords) {
		return;
	}

	if (selectedFieldData) {
		if (selectedFieldData->notes.empty()) {
			map->RemoveField(selectedFieldCoords);
		}
	}

	selectedFieldCoords = coords;

	selectedFieldData = map->GetField(coords);
	if (!selectedFieldData) {
		selectedFieldData = std::make_shared<Field>();
		map->AddField(coords, selectedFieldData);
	}
	Properties::Display(selectedFieldData);
}

void MapWorkspace::DrawOverlay(ImVec2 pos)
{
	ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;
	
	const float PAD = 10.0f;

	ImVec2 work_pos = pos;
	ImVec2 window_pos, window_pos_pivot;
	window_pos.x = (work_pos.x + PAD);
	window_pos.y = (work_pos.y + PAD);
	window_pos_pivot.x = 0.0f;
	window_pos_pivot.y = 0.0f;
	ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
	window_flags |= ImGuiWindowFlags_NoMove;
	
	ImGui::SetNextWindowBgAlpha(0.75f);
	if (ImGui::Begin("overlay", nullptr, window_flags))
	{
		if (ImGui::CollapsingHeader("Tools")) {
			if (ImGui::BeginListBox("##tools", ImVec2(150, 60))) {
				if (ImGui::Selectable("Move/Select", selectedTool == 0)) {
					selectedTool = 0;
				}

				if (ImGui::Selectable("Edit Areas", selectedTool == 1)) {
					selectedTool = 1;
				}

				ImGui::EndListBox();
			}

			if (ImGui::CollapsingHeader("Areas")) {
				if (ImGui::BeginListBox("##hide_areas", ImVec2(150, 80))) {
					int i = 0;
					int toRemove = -1;
					for (auto& area : map->areas) {
						std::string pre = "##" + area.name + "_";
						std::string checkbox_id = pre + "checkbox";
						ImGui::Checkbox(checkbox_id.c_str(), &area.visible);

						if (selectedTool == 1) {
							ImGui::SameLine();
							std::string button_id = "-" + pre + "button-";
							if (ImGui::Button(button_id.c_str())) {
								toRemove = i;
							}
						}

						ImGui::SameLine();

						if (ImGui::Selectable(area.name.c_str(), selectedArea == i)) {
							if (selectedArea == i) {
								selectedArea = -1;
							}
							else {
								selectedArea = i;
							}
						}

						i++;
					}
					if (toRemove >= 0 && toRemove < map->areas.size()) {
						map->areas.erase(std::next(map->areas.begin(), toRemove));
					}

					ImGui::EndListBox();
				}

				static std::string newAreaName = "";
				if (selectedTool == 1) {
					ImGui::SetNextItemWidth(150.0f);
					ImGui::InputText("##New area", &newAreaName);

					if (ImGui::Button("Add##add_new_area") && newAreaName != "") {
						if (std::find_if(map->areas.begin(), map->areas.end(), [&](auto& area) {
							return area.name == newAreaName;
							}) == map->areas.end()) {

							Map::Area newArea;
							newArea.name = newAreaName;
							newArea.visible = true;
							map->areas.push_back(newArea);
							newAreaName = "";

						}
					}
				}
			}
		}
	}
	ImGui::End();
}
