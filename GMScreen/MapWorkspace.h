#pragma once
#include "Window.h"
#include "Map.h"
#include "Texture.h"
#include <unordered_set>

class MapWorkspace : public Window {
public:
	MapWorkspace(std::string name, int uid);

	virtual void DrawContent() override;
	virtual void ProcessIO(ImGuiIO& io);
	virtual void Cleanup() override;

	void SetMap(std::string mapName);

protected:
	void OnFileLoad() override;

private:
	enum class State {
		Idle, Drag
	};

	std::shared_ptr<Map> map;
	std::shared_ptr<Texture> mapTexture;
	std::string shownViewTextureName;
	void UpdateView();

	std::shared_ptr<Field> selectedFieldData;
	glm::ivec2 selectedFieldCoords;
	void SelectField(glm::ivec2 coords);

	glm::vec2 mapOffset = glm::vec2(0.0f);
	glm::vec2 dragOffset = glm::vec2(0.0f);
	glm::vec2 mousePos = glm::vec2(0.0f);
	bool hovered = false;
	bool tileHovered = false;
	float scale = 1.0f;

	State state;
	glm::vec2 mapScreenPoint = glm::vec2(0.0f);

	glm::ivec2 hoveredTileCoords;

	int selectedTool = 0;
	void DrawOverlay(ImVec2 pos);
	int selectedArea = -1;
};