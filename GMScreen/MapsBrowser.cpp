#include "MapsBrowser.h"
#include "ResourcesManager.h"
#include <glm/glm.hpp>
#include <iostream>
#include "ImGUI/imgui_stdlib.h"
#include "Map.h"
#include "WindowManager.h"
#include "MapWorkspace.h"
#include "FileBrowsers.h"

MapsBrowser::MapsBrowser(std::string name, int uid)
    : Window(name, uid) {
}

void MapsBrowser::DrawContent() {
    if (ImGui::Button("+|New")) {
        ImGui::OpenPopup("New Map");
    }

    DrawMapsList();

    DrawNewMapWindow();
}

void MapsBrowser::Cleanup()
{
    vkDeviceWaitIdle(VulkanLoader::GetDevice());
    newMapSelectedTexture = nullptr;
}

void MapsBrowser::DrawMapsList()
{
    ImVec2 size = ImGui::GetWindowSize();
    size.y = 0;
    size.x -= 20;

    if (ImGui::BeginListBox("##maps_list", size)) {
        int i = 0;
        for (auto& p : ResourcesManager::maps) {

            if (ImGui::Selectable(p.first.c_str(), selectedMap == i)) {
                selectedMap = i;
                Properties::Display(p.second);
            }

            if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left)) {
                WindowManager::OpenWindow<MapWorkspace>(p.first);
            }

            ++i;
        }
        ImGui::EndListBox();
    }
}

void MapsBrowser::DrawNewMapWindow() {

    static ImGuiWindowFlags flags = ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoSavedSettings;
    const ImGuiViewport* viewport = ImGui::GetMainViewport();

    ImVec2 size = viewport->Size;
    size.x *= 0.8;
    size.y *= 0.9;
    ImGui::SetNextWindowSize(size);
    
    ImVec2 center = ImGui::GetMainViewport()->GetCenter();
    ImGui::SetNextWindowPos(center, 0, ImVec2(0.5f, 0.5f));

    if (ImGui::BeginPopupModal("New Map", nullptr, flags))
    {
        DrawNewMapOptions();
        DrawNewMapPreview();
        ImGui::EndPopup();
    }
}

void MapsBrowser::DrawNewMapOptions()
{
    ImGui::InputText("Name", &newMapName);

    ImGui::InputText("File", &newMapTexturePath, ImGuiInputTextFlags_ReadOnly);
    ImGui::SameLine();
    if (ImGui::Button("Select texture file")) {
        FileBrowsers::OpenBrowser.SetTypeFilters({ ".png", ".jpg" });
        FileBrowsers::OpenBrowser.Open();
    }

    FileBrowsers::OpenBrowser.Display();

    if (FileBrowsers::OpenBrowser.HasSelected())
    {
        newMapTexturePath = FileBrowsers::getLocalPath(FileBrowsers::OpenBrowser.GetSelected()).string();
        FileBrowsers::OpenBrowser.ClearSelected();
        FileBrowsers::OpenBrowser.Close();
        if (ResourcesManager::HasTexture(newMapTexturePath)) {
            newMapSelectedTexture = ResourcesManager::GetTexture(newMapTexturePath);
        }
        else {
            newMapSelectedTexture = ResourcesManager::CreateTexture(newMapTexturePath);
        }
    }

    if (newMapSelectedTexture) {
        newMapSize = newMapSelectedTexture->GetSize();
        ImGui::DragInt2("Size", &newMapSize[0], ImGuiInputTextFlags_ReadOnly);

        if (ImGui::DragFloat("Cell size", &newMapCellSize)) {
            newMapCellSize = glm::clamp(newMapCellSize, 10.0f, 200.0f);
        }

        if (ImGui::DragFloat2("Offset", &newMapCellOffset[0])) {
            if (newMapCellOffset.x < 0) newMapCellOffset.x = 0;
            if (newMapCellOffset.y < 0) newMapCellOffset.y = 0;
        }

        ImGui::ColorEdit4("Lines color", &newMapLinesColor[0]);
        ImGui::ColorEdit4("Selection color", &newMapSelectionColor[0]);

        ImGui::Separator();

        if (ImGui::Button("Add")) {
            if (newMapName.length() > 0) {

                MapData mapData{};
                mapData.views = { std::make_pair("Default", newMapTexturePath) };
                mapData.linesColor = newMapLinesColor;
                mapData.selectionColor = newMapSelectionColor;
                mapData.cellSize = newMapCellSize;
                mapData.cellOffset = newMapCellOffset;

                std::shared_ptr<Map> map = std::make_shared<Map>(
                    newMapName,
                    mapData,
                    newMapSize
                    );

                ResourcesManager::AddMap(map);

                if (!ResourcesManager::HasTexture(newMapTexturePath)) {
                    ResourcesManager::SaveTexture(newMapTexturePath, newMapSelectedTexture);
                }

                CloseNewMapWindow();
            }
        }
    }

    if (ImGui::Button("Cancel"))
        CloseNewMapWindow();
}

void MapsBrowser::DrawNewMapPreview()
{
    if (newMapSelectedTexture) {
        ImVec2 size = ImGui::GetWindowSize();
        glm::vec2 imageSize = newMapSelectedTexture->GetSize();
        float ratio = imageSize.y / imageSize.x;
        size.x -= 20;
        size.y = size.x * ratio;

        const ImVec2 p = ImGui::GetCursorScreenPos();
        ImGui::Image(newMapSelectedTexture->getDescriptor(), size);

        float scale = size.x / imageSize.x;
        ImDrawList* draw_list = ImGui::GetWindowDrawList();

        glm::vec2 minPos = newMapCellOffset;
        glm::vec2 maxPos = minPos - newMapCellSize;
        for (int d = minPos.x; d <= newMapSize.x; d += newMapCellSize) {
            maxPos.x = d;
        }
        for (int d = minPos.y; d <= newMapSize.y; d += newMapCellSize) {
            maxPos.y = d;
        }

        minPos *= scale;
        maxPos *= scale;
        float cellSize = newMapCellSize * scale;

        ImColor color(newMapLinesColor.r, newMapLinesColor.g, newMapLinesColor.b, newMapLinesColor.a);

        // horizontal lines
        for (float yPos = minPos.y; yPos < maxPos.y; yPos += cellSize) {
            ImVec2 p1(p.x+minPos.x, p.y + yPos), p2(p.x + maxPos.x, p.y + yPos);
            draw_list->AddLine(p1, p2, color, 2.0f);
        }
        draw_list->AddLine(ImVec2(p.x + minPos.x, p.y + maxPos.y), ImVec2(p.x + maxPos.x, p.y + maxPos.y), color, 2.0f);

        // vertical lines
        for (float xPos = minPos.x; xPos < maxPos.x; xPos += cellSize) {
            ImVec2 p1(p.x + xPos, p.y+minPos.y), p2(p.x + xPos, p.y + maxPos.y);
            draw_list->AddLine(p1, p2, color, 2.0f);
        }
        draw_list->AddLine(ImVec2(p.x + maxPos.x, p.y + minPos.y), ImVec2(p.x + maxPos.x, p.y + maxPos.y), color, 2.0f);

        // selection preview
        draw_list->AddRectFilled(ImVec2(p.x + minPos.x, p.y + minPos.y), ImVec2(p.x + minPos.x + cellSize, p.y + minPos.y + cellSize),
            ImColor(newMapSelectionColor.r, newMapSelectionColor.g, newMapSelectionColor.b, newMapSelectionColor.a));
    }
}

void MapsBrowser::CloseNewMapWindow() {
    vkDeviceWaitIdle(VulkanLoader::GetDevice());
    ImGui::CloseCurrentPopup();
    newMapName = "";
    newMapCellSize = 50;
    newMapCellOffset = glm::vec2(0);
    newMapSize = glm::ivec2(1000);
    newMapSelectedTexture = nullptr;
    newMapTexturePath = "choose a file...";
    newMapLinesColor = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
}
