#pragma once
#include "Window.h"
#include <glm/glm.hpp>
#include "Texture.h"
#include "imfilebrowser.h"
#include "Map.h"

class MapsBrowser : public Window {
public:
	MapsBrowser(std::string name, int uid);
	virtual void DrawContent() override;
	virtual void ProcessIO(ImGuiIO& io) {};
	virtual void Cleanup() override;
private:
	int selectedMap = -1;

	void DrawMapsList();

	void DrawNewMapWindow();
	void DrawNewMapOptions();
	void DrawNewMapPreview();
	void CloseNewMapWindow();

	std::string newMapName = "";
	float newMapCellSize = 50.0f;
	glm::vec2 newMapCellOffset = glm::vec2(0);
	glm::ivec2 newMapSize = glm::ivec2(1000);
	glm::vec4 newMapLinesColor = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
	glm::vec4 newMapSelectionColor = glm::vec4(1.0f, 0.0f, 0.0f, 0.5f);
	std::string newMapTexturePath = "choose a file...";
	std::vector<std::pair<std::string, std::string>> newMapViews;

	std::shared_ptr<Texture> newMapSelectedTexture;
};