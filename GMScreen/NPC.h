#pragma once
#include <string>
#include <vector>
#include <cereal/access.hpp>

class NPC 
{
public:
	NPC(std::string name) 
	: name(name), textureName("") {}

	std::string name;
	std::string textureName;
	std::vector<std::string> tags;
	std::vector<std::pair<std::string, std::string>> info;

private:
	friend class cereal::access;
	NPC()
	: name(""), textureName("") {}
	template<class Archive>
	void serialize(Archive& ar) {
		ar(name, textureName, tags, info);
	}
};