#include "NPCBrowser.h"
#include "ResourcesManager.h"
#include "WindowManager.h"
#include "ImGUI/imgui_stdlib.h"
#include "StringSearch.h"
#include "NPCViewer.h"

NPCBrowser::NPCBrowser(std::string name, int uid)
	: Window(name, uid)
{
}

void NPCBrowser::DrawContent()
{
    ImGui::InputText("##npc_searchbar", &filter);

    ImVec2 size = ImGui::GetWindowSize();
    size.y = 0;
    size.x -= 20;

    if (ImGui::BeginListBox("##npcs_list", size)) {
        int i = 0;
        for (std::shared_ptr<NPC>& npc : ResourcesManager::npcs) {
            bool found = filter == "";
            std::string searchTagUsed = "";
            if (!found) {
                found = find(npc->name, filter);
            }

            if (!found) {
                for (std::string& tag : npc->tags) {
                    found = find(tag, filter);
                    if (found) {
                        searchTagUsed = tag;
                        break;
                    }
                }
            }

            if (!found) {
                continue;
            }

            std::string name = npc->name;
            if (searchTagUsed != "") {
                name = "[" + searchTagUsed + "] " + name;
            }
            if (ImGui::Selectable(name.c_str(), selected == i)) {
                selected = i;
            }

            if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left)) {
                WindowManager::OpenWindow<NPCViewer>(npc->name);
            }

            ++i;
        }
        ImGui::EndListBox();
    }

    ImGui::InputText("##new_name", &newName);
    if (ImGui::Button("Add##add_npc") && newName != "") {
        ResourcesManager::npcs.push_back(std::make_shared<NPC>(newName));
        newName = "";
    }
}