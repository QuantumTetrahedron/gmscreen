#pragma once
#include "Window.h"

class NPCBrowser : public Window {
public:
	NPCBrowser(std::string name, int uid);
	virtual void DrawContent() override;
	virtual void ProcessIO(ImGuiIO& io) {};
private:
	int selected = -1;
	std::string filter;
	std::string newName;
};