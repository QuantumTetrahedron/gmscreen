#include "NPCViewer.h"
#include "ResourcesManager.h"
#include "ImGUI/imgui_stdlib.h"
#include "FileBrowsers.h"

NPCViewer::NPCViewer(std::string name, int uid)
	: Window(name, uid)
{
	startDocked = true;
	SetWindowFlags(GetWindowFlags() | ImGuiWindowFlags_NoSavedSettings);

	for (auto& p : ResourcesManager::npcs) {
		if (p->name == name) {
			npc = p;
			break;
		}
	}
}

void NPCViewer::DrawContent()
{
	if (!npc) {
		return;
	}

	ImGui::InputText("Name", &npc->name);

	ImGui::Separator();

	// tags
	auto draw_list = ImGui::GetWindowDrawList();
	std::string tagToRemove;

	float tag_height = 25.0f;
	float tag_remove_button_width = 20.0f;

	float maxWidth = ImGui::GetWindowWidth() * 0.7;
	float currWidth = 0;
	float spacing = 5.0f;
	float padding = 2.0f;

	for (auto& tag : npc->tags) {
		ImVec2 text_size = ImGui::CalcTextSize(tag.c_str());
		ImVec2 size = text_size;
		size.y = tag_height;
		size.x += tag_remove_button_width + 2 * padding;

		if (currWidth > 0) {
			if (currWidth + size.x + spacing > maxWidth) {
				currWidth = 0;
			}
			else {
				ImGui::SameLine(0.0f, spacing);
				currWidth += spacing;
			}
		}

		ImGui::Dummy(ImVec2(0, 0));
		ImVec2 min = ImGui::GetItemRectMin();
		draw_list->AddRectFilled(min, ImVec2(min.x + size.x, min.y + size.y), ImColor(0.4f, 0.4f, 0.4f, 1.0f), 10.0f);
		draw_list->AddText(ImVec2(min.x + padding, min.y + (size.y/2) - (text_size.y/2)), ImColor(1.0f, 1.0f, 1.0f, 1.0f), tag.c_str());
		ImGui::SameLine(0.0f, padding);
		ImGui::Dummy(ImVec2(text_size.x, size.y));

		ImGui::SameLine(0.0f, padding);
		if (ImGui::Button(("x##npc_remove_tag_" + tag).c_str(), ImVec2(tag_remove_button_width, tag_height))) {
			tagToRemove = tag;
		}

		currWidth += size.x;
	}
	if (tagToRemove != "") {
		npc->tags.erase(std::find(npc->tags.begin(), npc->tags.end(), tagToRemove));
		tagToRemove = "";
	}

	ImGui::InputText("##new_tag_name", &newTagName);
	ImGui::SameLine();
	if (ImGui::Button("+##add_new_tag") && newTagName != "") {
		if (std::find(std::begin(npc->tags), std::end(npc->tags), newTagName) == std::end(npc->tags)) {
			npc->tags.push_back(newTagName);
		}
		newTagName = "";
	}

	// image
	if (!image && npc->textureName != "") {
		image = ResourcesManager::GetTexture(npc->textureName);
	}
	std::string buttonName = "Add image";
	if (image) {
		ImVec2 size = ImGui::GetWindowSize();
		size.x -= 20;
		glm::vec2 imageSize = image->GetSize();
		if (imageSize.x > size.x) {
			float ratio = imageSize.y / imageSize.x;
			size.y = size.x * ratio;
		}
		else {
			size = ImVec2(imageSize.x, imageSize.y);
		}
		ImGui::Image(image->getDescriptor(), size);

		buttonName = "Change";
	}
	ImGui::Separator();
	if (ImGui::Button((buttonName + "##add_npc_image").c_str())) {
		FileBrowsers::OpenBrowser.SetTypeFilters({ ".png", ".jpg" });
		FileBrowsers::OpenBrowser.Open();
	}
	if (image) {
		ImGui::SameLine();
		if (ImGui::Button("Remove##remove_npc_image")) {
			image = nullptr;
			npc->textureName = "";
		}
	}

	FileBrowsers::OpenBrowser.Display();

	if (FileBrowsers::OpenBrowser.HasSelected())
	{
		std::string texturePath = FileBrowsers::getLocalPath(FileBrowsers::OpenBrowser.GetSelected()).string();
		FileBrowsers::OpenBrowser.ClearSelected();
		FileBrowsers::OpenBrowser.Close();
		if (ResourcesManager::HasTexture(texturePath)) {
			image = ResourcesManager::GetTexture(texturePath);
		}
		else {
			image = ResourcesManager::CreateTexture(texturePath);
			ResourcesManager::SaveTexture(texturePath, image);
		}
		npc->textureName = texturePath;
	}

	// data
	int toRemove = -1;
	int noteNum = 0;
	for (auto& note : npc->info) {
		std::string buttonId = "-##-" + std::to_string(noteNum);
		if (ImGui::Button(buttonId.c_str())) {
			toRemove = noteNum;
		}
		ImGui::SameLine();
		std::string headerName = note.first + "##" + note.first + std::to_string(noteNum);
		if (ImGui::CollapsingHeader(headerName.c_str())) {
			std::string descName = "##noteDesc" + std::to_string(noteNum);
			ImGui::InputTextMultiline(descName.c_str(), &note.second);
		}
		noteNum++;
	}

	if (toRemove >= 0) {
		noteNum = 0;
		npc->info.erase(npc->info.begin() + noteNum);
	}

	ImGui::Separator();
	ImGui::InputText("##new", &newInfoTitle);
	ImGui::SameLine();
	if (ImGui::Button("+")) {
		if (newInfoTitle != "") {
			npc->info.push_back(std::make_pair(newInfoTitle, std::string("")));
			newInfoTitle = "";
		}
	}
}

void NPCViewer::Cleanup()
{
	vkDeviceWaitIdle(VulkanLoader::GetDevice());
	image = nullptr;
}
