#pragma once
#include "Window.h"
#include "NPC.h"
#include "Texture.h"

class NPCViewer : public Window {
public:
	NPCViewer(std::string name, int uid);
	virtual void DrawContent() override;
	virtual void ProcessIO(ImGuiIO& io) {};
	virtual void Cleanup() override;
private:
	std::shared_ptr<NPC> npc;
	std::string newTagName;
	std::shared_ptr<Texture> image;
	std::string newInfoTitle;
};