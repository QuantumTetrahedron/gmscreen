#pragma once
#include <string>
#include <vector>
#include <map>
#include <variant>
#include <cereal/access.hpp>

class Player {
public:
	Player(std::string n) : name(n) {
	}

	std::string name;

	typedef std::variant<int, float, bool, std::string> var;

	std::map<std::string, var> data;
	std::map<std::string, std::vector<var>> vectors;

	template <typename T>
	T& get(std::string varName, T initialValue = T()) {
		if (data.find(varName) == data.end()) {
			data[varName] = initialValue;
		}

		return std::get<T>(data[varName]);
	}

	template <typename T>
	T* getV(std::string vecName, int index) {
		if (vectors.find(vecName) == vectors.end()) {
			vectors[vecName] = std::vector<var>();
		}

		if (index < 0 || index >= vectors[vecName].size()) {
			return nullptr;
		}
		return &std::get<T>(vectors[vecName][index]);
	}

	template <typename T>
	void addV(std::string vecName, T value) {
		if (vectors.find(vecName) == vectors.end()) {
			vectors[vecName] = std::vector<var>();
		}

		vectors[vecName].push_back(value);
	}

	void eraseV(std::string vecName, int index) {
		if (vectors.find(vecName) == vectors.end()) {
			return;
		}
		vectors[vecName].erase(vectors[vecName].begin() + index);
	}

private:
	friend class cereal::access;

	Player() : name(""){}

	template <class Archive>
	void serialize(Archive& ar) {
		ar(name, data, vectors);
	}
};