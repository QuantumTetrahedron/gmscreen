#include "PlayersBrowser.h"
#include "ResourcesManager.h"
#include "WindowManager.h"
#include "ImGUI/imgui_stdlib.h"
#include "CharacterSheet.h"

PlayersBrowser::PlayersBrowser(std::string name, int uid)
	: Window(name, uid)
{
}

void PlayersBrowser::DrawContent()
{
    ImGui::InputText("Name", &newPlayerName);
    if (ImGui::Button("Add") && newPlayerName != "") {
        ResourcesManager::players.push_back(std::make_shared<Player>(newPlayerName));
        newPlayerName = "";
    }

    ImGui::Separator();

    ImVec2 size = ImGui::GetWindowSize();
    size.y = 0;
    size.x -= 20;

    if (ImGui::BeginListBox("##maps_list", size)) {
        int i = 0;
        for (auto& p : ResourcesManager::players) {

            if (ImGui::Selectable(p->name.c_str(), selectedPlayer == i)) {
                selectedPlayer = i;
            }

            if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left)) {
                WindowManager::OpenWindow<CharacterSheet>(p->name);
            }

            ++i;
        }
        ImGui::EndListBox();
    }
}
