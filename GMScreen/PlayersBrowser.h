#pragma once

#include "Window.h"

class PlayersBrowser : public Window {
public:
	PlayersBrowser(std::string name, int uid);
	virtual void DrawContent() override;
	virtual void ProcessIO(ImGuiIO& io) {};
private:
	int selectedPlayer;
	std::string newPlayerName;
};