#include "Properties.h"

std::weak_ptr<IDisplayable> Properties::displayed;

Properties::Properties(std::string name, int uid)
    : Window(name, uid)
{
    displayed.reset();
}

void Properties::DrawContent() {
    std::shared_ptr<IDisplayable> locked = displayed.lock();
    if (locked) {
        locked->Display();
    }
    else {
        displayed.reset();
    }
}

void Properties::ProcessIO(ImGuiIO& io) {

}