#pragma once
#include "Window.h"
#include <variant>
#include <memory>

class IDisplayable {
public:
	virtual void Display() = 0;
};

class Properties : public Window
{
public:
	Properties(std::string name, int uid);
	void DrawContent() override;
	void ProcessIO(ImGuiIO& io) override;

	static void Display(std::weak_ptr<IDisplayable> toDisplay) {
		displayed = toDisplay;
	}

protected:
	void OnFileLoad() override {
		displayed.reset();
	}

private:
	static std::weak_ptr<IDisplayable> displayed;
};

