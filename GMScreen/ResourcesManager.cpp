#include "ResourcesManager.h"

std::map<std::string, std::shared_ptr<Map>> ResourcesManager::maps;
std::map<std::string, std::shared_ptr<Texture>> ResourcesManager::textures;
std::vector<std::shared_ptr<Player>> ResourcesManager::players;
std::vector<std::shared_ptr<NPC>> ResourcesManager::npcs;
std::vector<std::shared_ptr<Chart>> ResourcesManager::charts;
Settings ResourcesManager::settings;

std::shared_ptr<Map> ResourcesManager::GetMap(std::string name)
{
	return maps.at(name);
}

void ResourcesManager::AddMap(std::shared_ptr<Map> map)
{
	maps[map->GetName()] = map;
}

void ResourcesManager::DeleteMap(std::string name)
{
	maps.erase(name);
}

std::shared_ptr<Texture> ResourcesManager::GetTexture(std::string name)
{
	return textures.at(name);
}

std::shared_ptr<Texture> ResourcesManager::CreateTexture(std::string path)
{
	return std::make_shared<Texture>(path.c_str());
}

bool ResourcesManager::HasTexture(std::string name)
{
	return textures.find(name) != textures.end();
}

void ResourcesManager::SaveTexture(std::string name, std::shared_ptr<Texture> tex)
{
	textures[name] = tex;
}

void ResourcesManager::DeleteTexture(std::string name)
{
	textures.erase(name);
}

void ResourcesManager::Cleanup()
{
	textures.clear();
	maps.clear();
}

void ResourcesManager::RemoveUnusedTextures()
{
	std::vector<std::string> toRemove;
	for (auto& p : ResourcesManager::textures) {
		const std::string& textureName = p.first;
		std::shared_ptr<Texture>& texture = p.second;
		bool found = false;

		for (auto& map_p : ResourcesManager::maps) {
			const auto& views = map_p.second->GetData().views;
			for (const auto& v : views) {
				const std::string& viewTextureName = v.second;
				if (viewTextureName == textureName) {
					found = true;
					break;
				}
			}
			if (found) break;
		}
		if (found) continue;

		for (auto& npc : ResourcesManager::npcs) {
			if (npc->textureName == textureName) {
				found = true;
				break;
			}
		}

		if (!found) {
			toRemove.push_back(textureName);
		}
	}

	for (std::string& r : toRemove) {
		ResourcesManager::textures.erase(r);
	}
}
