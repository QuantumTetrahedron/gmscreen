#pragma once
#include <string>
#include <map>
#include <memory>
#include "Map.h"
#include "Texture.h"
#include "Player.h"
#include "Settings.h"
#include "NPC.h"
#include "Chart.h"

class ResourcesManager {
public:
	static std::shared_ptr<Map> GetMap(std::string name);
	static void AddMap(std::shared_ptr<Map> map);
	static void DeleteMap(std::string name);
	static std::map<std::string, std::shared_ptr<Map>> maps;

	static std::vector<std::shared_ptr<Player>> players;

	static std::shared_ptr<Texture> GetTexture(std::string name);
	static std::shared_ptr<Texture> CreateTexture(std::string path);
	static bool HasTexture(std::string name);
	static void SaveTexture(std::string name, std::shared_ptr<Texture> tex);
	static void DeleteTexture(std::string name);
	static std::map<std::string, std::shared_ptr<Texture>> textures;

	static std::vector<std::shared_ptr<NPC>> npcs;
	static std::vector<std::shared_ptr<Chart>> charts;

	static void Cleanup();

	static Settings settings;

	static void RemoveUnusedTextures();
};