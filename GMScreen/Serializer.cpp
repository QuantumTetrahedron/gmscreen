#include "Serializer.h"
#include <fstream>
#include <iostream>
#include <algorithm>
#include <variant>
#include <sstream>

#include "ResourcesManager.h"
#include <cereal/archives/json.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/utility.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/tuple.hpp>
#include <cereal/types/variant.hpp>

void Serializer::Serialize(const char* file)
{
    std::cout << "Saving to file: " << file << std::endl;

    std::ofstream f(file, std::ios::trunc | std::ios::out | std::ios::binary);

    cereal::BinaryOutputArchive archive(f);

    ResourcesManager::RemoveUnusedTextures();
    archive(cereal::make_nvp("settings", ResourcesManager::settings));
    archive(cereal::make_nvp("textures", ResourcesManager::textures));
    archive(cereal::make_nvp("maps", ResourcesManager::maps));
    archive(cereal::make_nvp("players", ResourcesManager::players));
    archive(cereal::make_nvp("npcs", ResourcesManager::npcs));
}

void Serializer::Deserialize(const char* path)
{
    std::cout << "Opening file: " << path << std::endl;

    std::ifstream file(path, std::ios::in | std::ios::binary);

    cereal::BinaryInputArchive archive(file);

    archive(cereal::make_nvp("settings", ResourcesManager::settings));
    archive(cereal::make_nvp("textures", ResourcesManager::textures));
    archive(cereal::make_nvp("maps", ResourcesManager::maps));
    archive(cereal::make_nvp("players", ResourcesManager::players));
    archive(cereal::make_nvp("npcs", ResourcesManager::npcs));
}