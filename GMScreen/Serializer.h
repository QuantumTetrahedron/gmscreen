#pragma once

#include <fstream>
#include <vector>
#include <map>

class ISerializable {
public:
	virtual void Serialize(std::ofstream& out) = 0;
};

class Serializer {
public:
	void Serialize(const char* file);
	void Deserialize(const char* file);
};