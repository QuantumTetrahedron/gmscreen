#pragma once
#include <string>
#include <cereal/access.hpp>
#include "CharacterSheet.h"

class Settings {
public:

	std::string characterSheetScriptFileName = "";

private:
	friend class cereal::access;

	template <class Archive>
	void save(Archive& ar) const {
		ar(characterSheetScriptFileName);
	}

	template <class Archive>
	void load(Archive& ar) {
		ar(characterSheetScriptFileName);
		CharacterSheet::SetConfigFile(characterSheetScriptFileName);
	}
};