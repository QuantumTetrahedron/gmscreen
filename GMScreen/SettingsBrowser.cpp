#include "SettingsBrowser.h"
#include "ImGUI/imgui_stdlib.h"
#include "FileBrowsers.h"
#include "CharacterSheet.h"
#include "ResourcesManager.h"

SettingsBrowser::SettingsBrowser(std::string name, int uid)
	: Window(name, uid)
{
	
}

void SettingsBrowser::DrawContent()
{
	ImGui::BeginChild("##settings_categories", ImVec2(200, 0), true);

	if (ImGui::Selectable("General", openCategory == Category::General)) {
		openCategory = Category::General;
	}

	if (ImGui::Selectable("Character Sheets", openCategory == Category::CharacterSheets)) {
		openCategory = Category::CharacterSheets;
	}

	ImGui::EndChild();

	ImGui::SameLine();

	Settings& settings = ResourcesManager::settings;
	ImGui::BeginGroup();
	if (openCategory == Category::General) {
		DrawGeneralSettings(settings);
	}
	else if (openCategory == Category::CharacterSheets) {
		DrawCharacterSheetsSettings(settings);
	}
	ImGui::EndGroup();
}

void SettingsBrowser::DrawGeneralSettings(Settings& settings)
{
	ImGui::Text("General");

	ImGui::Dummy(ImVec2(0, 5));
	ImGui::Separator();
	ImGui::Dummy(ImVec2(0, 5));
}

void SettingsBrowser::DrawCharacterSheetsSettings(Settings& settings)
{
	ImGui::Text("Character Sheets");

	ImGui::Dummy(ImVec2(0, 5));
	ImGui::Separator();
	ImGui::Dummy(ImVec2(0, 5));

	ImGui::Text("Config file");
	
	ImGui::SameLine();
	ImGui::TextDisabled("(?)");
	if (ImGui::IsItemHovered())
	{
		ImGui::BeginTooltip();
		ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
		ImGui::TextUnformatted("Character Sheets are configured using Lua Script. See lua samples for more help");
		ImGui::PopTextWrapPos();
		ImGui::EndTooltip();
	}

	ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.8);
	if (settings.characterSheetScriptFileName == "") {
		std::string chooseText = "choose a file...";
		ImGui::InputText("##configfile", &chooseText, ImGuiInputTextFlags_ReadOnly);
	}
	else {
		ImGui::InputText("##configfile", &settings.characterSheetScriptFileName, ImGuiInputTextFlags_ReadOnly);
	}
	//ImGui::SameLine();
	if (ImGui::Button("Select File")) {
		FileBrowsers::OpenBrowser.ClearSelected();
		FileBrowsers::OpenBrowser.SetTypeFilters({ });
		FileBrowsers::OpenBrowser.SetTypeFilters({ ".lua" });
		FileBrowsers::OpenBrowser.Open();
	}

	FileBrowsers::OpenBrowser.Display();

	if (FileBrowsers::OpenBrowser.HasSelected())
	{
		settings.characterSheetScriptFileName = FileBrowsers::OpenBrowser.GetSelected().string();
		FileBrowsers::OpenBrowser.ClearSelected();
		FileBrowsers::OpenBrowser.Close();

		CharacterSheet::SetConfigFile(settings.characterSheetScriptFileName);
	}
}
