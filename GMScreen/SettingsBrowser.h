#pragma once
#include "Window.h"
#include "Settings.h"

class SettingsBrowser : public Window {
public:
	SettingsBrowser(std::string name, int uid);

	virtual void DrawContent() override;
	virtual void ProcessIO(ImGuiIO& io) {}

	enum class Category {
		General, CharacterSheets
	};

	Category openCategory = Category::General;

private:
	void DrawGeneralSettings(Settings& settings);
	void DrawCharacterSheetsSettings(Settings& settings);
};