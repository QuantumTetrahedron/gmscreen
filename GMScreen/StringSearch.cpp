#include "StringSearch.h"
#include <algorithm>
#include <string>
#include <cctype>
#include <functional>

bool find(const std::string& str, const std::string& substr, bool caseSenitive)
{
    auto it = std::search(
        str.begin(), str.end(),
        substr.begin(), substr.end(),
        [&](char ch1, char ch2) { 
            if (caseSenitive) {
                return ch1 == ch2;
            }
            return std::toupper(ch1) == std::toupper(ch2); 
        }
    );
    return (it != str.end());

}
