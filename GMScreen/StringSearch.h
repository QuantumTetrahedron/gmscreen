#pragma once
#include <string>

bool find(const std::string& str, const std::string& substr, bool caseSenitive = false);