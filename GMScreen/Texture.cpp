#include "Texture.h"
#include <stdexcept>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <vector>
#include <iostream>
#include <fstream>

Texture::Texture(const char* filename) {
	auto rawData = loadRawFile(filename);
	create(rawData);
}

Texture::~Texture() {
	cleanup();
}

void Texture::create(const std::vector<unsigned char>& rawData) {
	base64_data = base64::encode(rawData.data(), rawData.size());

	int w, h, ch;
	auto pixels = stbi_load_from_memory(rawData.data(), rawData.size(), &w, &h, &ch, STBI_rgb_alpha);

	if (!pixels) {
		throw std::runtime_error("failed to load texture");
	}

	size.x = w;
	size.y = h;
	channels = ch;

	VkDeviceSize imageSize = w * h * 4;

	Buffer staggingBuffer;
	staggingBuffer.create(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

	void* data;
	vkMapMemory(VulkanLoader::GetDevice(), staggingBuffer.getMemory(), 0, imageSize, 0, &data);
	memcpy(data, pixels, static_cast<size_t>(imageSize));
	vkUnmapMemory(VulkanLoader::GetDevice(), staggingBuffer.getMemory());

	stbi_image_free(pixels);

	image.create(w, h, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	image.transitionLayout(VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
	image.copyBuffer(staggingBuffer);
	image.transitionLayout(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

	staggingBuffer.cleanup();

	imageView = image.createImageView();

	descriptorPool.create(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1, 1);
	createDescriptorSet(TexturesData::GetLayout(), TexturesData::GetSampler());
}

void Texture::createDescriptorSet(VkDescriptorSetLayout layout, VkSampler textureSampler)
{
	std::vector<VkDescriptorSetLayout> layouts(1, layout);
	descriptorPool.Allocate(1, layouts.data(), &descriptorSet);

	std::vector<VkDescriptorImageInfo> imageInfos;

	VkDescriptorImageInfo info{};
	info.imageView = imageView;
	
	info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	info.sampler = textureSampler;
	imageInfos.push_back(info);

	VkWriteDescriptorSet descriptorWrite{};
	descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	descriptorWrite.dstSet = descriptorSet;
	descriptorWrite.dstBinding = 0;
	descriptorWrite.dstArrayElement = 0;
	descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	descriptorWrite.descriptorCount = imageInfos.size();
	descriptorWrite.pImageInfo = imageInfos.data();

	vkUpdateDescriptorSets(VulkanLoader::GetDevice(), 1, &descriptorWrite, 0, nullptr);
}

void Texture::cleanup()
{
	vkDestroyImageView(VulkanLoader::GetDevice(), imageView, nullptr);
	image.cleanup();
	descriptorPool.cleanup();
}

std::vector<unsigned char> Texture::loadRawFile(std::string path)
{
	stbi_set_flip_vertically_on_load(false);

	std::vector<unsigned char> raw_data;
	std::streampos rawSize;
	std::ifstream file(path.c_str(), std::ios::binary | std::ios::ate);
	if (file.is_open()) {
		rawSize = file.tellg();
		file.seekg(0, std::ios::beg);
		raw_data.resize(rawSize);
		file.read((char*) (&raw_data[0]), rawSize);
		file.close();
	}
	return raw_data;
}

VkImageView Texture::getImageView() const
{
	return imageView;
}


VkDescriptorSetLayout TexturesData::layout;
VkSampler TexturesData::sampler;
bool TexturesData::initialized = false;
bool TexturesData::cleaned = false;

void TexturesData::Init()
{
	if (initialized) {
		return;
	}
	initialized = true;

	VkDescriptorSetLayoutBinding samplersBinding{};
	samplersBinding.binding = 0;
	samplersBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	samplersBinding.descriptorCount = 1;
	samplersBinding.pImmutableSamplers = nullptr;
	samplersBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	VkDescriptorSetLayoutCreateInfo meshLayoutInfo{};
	meshLayoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	meshLayoutInfo.bindingCount = 1;
	meshLayoutInfo.pBindings = &samplersBinding;

	vkCreateDescriptorSetLayout(VulkanLoader::GetDevice(), &meshLayoutInfo, nullptr, &layout);

	VkSamplerCreateInfo samplerInfo{};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.minFilter = VK_FILTER_LINEAR;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
	samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
	samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
	samplerInfo.anisotropyEnable = VK_FALSE;
	samplerInfo.maxAnisotropy = 16.0f;
	samplerInfo.borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
	samplerInfo.unnormalizedCoordinates = VK_FALSE;
	samplerInfo.compareEnable = VK_FALSE;
	samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerInfo.mipLodBias = 0.0f;
	samplerInfo.minLod = 0.0f;
	samplerInfo.maxLod = 0.0f;

	vkCreateSampler(VulkanLoader::GetDevice(), &samplerInfo, nullptr, &sampler);
}

void TexturesData::Cleanup() 
{
	vkDestroyDescriptorSetLayout(VulkanLoader::GetDevice(), layout, nullptr);
	vkDestroySampler(VulkanLoader::GetDevice(), sampler, nullptr);
}

VkDescriptorSetLayout TexturesData::GetLayout()
{
	return layout;
}

VkSampler TexturesData::GetSampler()
{
	return sampler;
}
