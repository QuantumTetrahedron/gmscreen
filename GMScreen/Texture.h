#pragma once
#include "Image.h"
#include "DescriptorPool.h"
#include <glm/glm.hpp>
#include <string>
#include <cereal/access.hpp>
#include "base64.h"

class TexturesData {
public:
	static void Init();
	static void Cleanup();

	static VkDescriptorSetLayout GetLayout();
	static VkSampler GetSampler();

private:
	static VkDescriptorSetLayout layout;
	static VkSampler sampler;

	static bool initialized;
	static bool cleaned;
};

class Texture
{
public:
	Texture(const char* filename);
	~Texture();

	VkImageView getImageView() const;
	Image image;

	VkDescriptorSet getDescriptor() {
		return descriptorSet;
	}

	glm::vec2 GetSize() {
		return size;
	}

private:
	Texture() {}

	void create(const std::vector<unsigned char>& rawData);
	void createDescriptorSet(VkDescriptorSetLayout layout, VkSampler textureSampler);

	void cleanup();

	VkImageView imageView;

	VkDescriptorSet descriptorSet;
	DescriptorPool descriptorPool;

	glm::ivec2 size;
	int channels;
	std::string base64_data;

	friend class cereal::access;

	std::vector<unsigned char> loadRawFile(std::string path);

	template <class Archive>
	void save(Archive& ar) const {
		ar(base64_data);
	}

	template <class Archive>
	void load(Archive& ar) {

		std::vector<unsigned char> pixels;
		
		std::string b64;
		ar(b64);
		
		pixels = base64::decode(b64);

		create(pixels);
	}
};