#include "TopMenu.h"
#include <iostream>
#include "WindowManager.h"
#include "MapsBrowser.h"
#include "FileBrowsers.h"
#include "ResourcesManager.h"
#include "PlayersBrowser.h"
#include "SettingsBrowser.h"
#include "NPCBrowser.h"
#include "ChartsBrowser.h"

void TopMenu::Draw()
{
    if (ImGui::BeginMainMenuBar())
    {
        if (ImGui::BeginMenu("Campaign")) {
            if (ImGui::MenuItem("Open")) {

                FileBrowsers::OpenBrowser.ClearSelected();
                FileBrowsers::OpenBrowser.SetTypeFilters({ });
                FileBrowsers::OpenBrowser.SetTypeFilters({ ".cmp" });

                FileBrowsers::OpenBrowser.Open();
            }
            if (openedFilePath != "") {
                if (ImGui::MenuItem("Save", openedFilePath.c_str())) {
                    serializer.Serialize(openedFilePath.c_str());
                }
            }
            else {
                ImGui::BeginDisabled();
                ImGui::MenuItem("Save");
                ImGui::EndDisabled();
            }
            if(ImGui::MenuItem("Save as...")) {
                FileBrowsers::OpenBrowser.ClearSelected();
                FileBrowsers::OpenBrowser.SetTypeFilters({ });
                FileBrowsers::OpenBrowser.SetTypeFilters({ ".cmp" });
                FileBrowsers::SaveBrowser.Open();
            }

            ImGui::Separator();
            if (ImGui::MenuItem("Settings")) {
                WindowManager::OpenWindow<SettingsBrowser>("Settings");
            }

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Windows")) {
            if (ImGui::MenuItem("Maps")) {
                WindowManager::OpenWindow<MapsBrowser>("Maps Browser");
            }
            if (ImGui::MenuItem("Players")) {
                WindowManager::OpenWindow<PlayersBrowser>("Players");
            }
            if (ImGui::MenuItem("NPCs")) {
                WindowManager::OpenWindow<NPCBrowser>("NPCs");
            }
            if (ImGui::MenuItem("Charts")) {
                WindowManager::OpenWindow<ChartsBrowser>("Charts Browser");
            }
            if (ImGui::MenuItem("Tables")) {

            }
            if (ImGui::MenuItem("Properties")) {
                WindowManager::OpenWindow<Properties>("Properties");
            }
            ImGui::EndMenu();
        }

        ImGui::EndMainMenuBar();
    }

    FileBrowsers::SaveBrowser.Display();
    FileBrowsers::OpenBrowser.Display();

    if (FileBrowsers::SaveBrowser.HasSelected())
    {
        openedFilePath = FileBrowsers::SaveBrowser.GetSelected().string();
        serializer.Serialize(openedFilePath.c_str());
        FileBrowsers::SaveBrowser.ClearSelected();
    }

    if (FileBrowsers::OpenBrowser.HasSelected())
    {
        openedFilePath = FileBrowsers::OpenBrowser.GetSelected().string();
        WindowManager::OnFileLoad();
        ResourcesManager::Cleanup();
        serializer.Deserialize(openedFilePath.c_str());
        FileBrowsers::OpenBrowser.ClearSelected();
        FileBrowsers::OpenBrowser.Close();
    }
}
