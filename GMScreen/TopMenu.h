#pragma once
#include "Serializer.h"
#include "Workspace.h"
#include <string>

class TopMenu {
public:
	void Draw();

private:
	std::string openedFilePath;
	Serializer serializer;
};