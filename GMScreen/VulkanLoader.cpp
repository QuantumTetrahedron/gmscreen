#include "VulkanLoader.h"
#include "CommandPool.h"
#include "ResourcesManager.h"

VkAllocationCallbacks* VulkanLoader::allocator = nullptr;
VkInstance VulkanLoader::instance = VK_NULL_HANDLE;
VkPhysicalDevice VulkanLoader::physicalDevice = VK_NULL_HANDLE;
VkDevice VulkanLoader::device = VK_NULL_HANDLE;

uint32_t VulkanLoader::queueFamily = (uint32_t)-1;
VkQueue VulkanLoader::queue = VK_NULL_HANDLE;

VkDebugReportCallbackEXT VulkanLoader::debugReport = VK_NULL_HANDLE;

VkPipelineCache VulkanLoader::pipelineCache = VK_NULL_HANDLE;
VkDescriptorPool VulkanLoader::descriptorPool = VK_NULL_HANDLE;

ImGui_ImplVulkanH_Window VulkanLoader::mainWindowData;

int VulkanLoader::minImageCount = 2;
bool VulkanLoader::swapchainRebuild = false;

void VulkanLoader::Setup(GLFWwindow* window, const char** extensions, uint32_t extensionsCount)
{
	CreateVulkanInstance(extensions, extensionsCount);
	SelectGPU();
	SelectGraphicsQueueFamily();
	CreateLogicalDevice();
	CreateDescriptorPool();

	VkSurfaceKHR surface;
	VkResult err = glfwCreateWindowSurface(instance, window, allocator, &surface);
	CheckVkResult(err);

	int w, h;
	glfwGetFramebufferSize(window, &w, &h);
	ImGui_ImplVulkanH_Window* wd = &mainWindowData;

	SetupVulkanWindow(wd, surface, w, h);

	CommandPool::create();
	TexturesData::Init();
}

void VulkanLoader::InitImGui(GLFWwindow* window)
{
	ImGui_ImplGlfw_InitForVulkan(window, true);
	ImGui_ImplVulkan_InitInfo init_info = {};
	init_info.Instance = instance;
	init_info.PhysicalDevice = physicalDevice;
	init_info.Device = device;
	init_info.QueueFamily = queueFamily;
	init_info.Queue = queue;
	init_info.PipelineCache = pipelineCache;
	init_info.DescriptorPool = descriptorPool;
	init_info.Allocator = allocator;
	init_info.MinImageCount = minImageCount;
	init_info.ImageCount = mainWindowData.ImageCount;
	init_info.CheckVkResultFn = CheckVkResult;
	ImGui_ImplVulkan_Init(&init_info, mainWindowData.RenderPass);
}

void VulkanLoader::LoadFonts(ImGuiIO& io)
{
	// Load Fonts
    // - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
    // - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
    // - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
    // - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
    // - Read 'docs/FONTS.md' for more instructions and details.
    // - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
    //io.Fonts->AddFontDefault();
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
    //ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
    //IM_ASSERT(font != NULL);

	ImFontGlyphRangesBuilder b;
	b.AddRanges(io.Fonts->GetGlyphRangesDefault());
	b.AddChar(0x0104); // �
	b.AddChar(0x0105); // �
	b.AddChar(0x0106); // �
	b.AddChar(0x0107); // �
	b.AddChar(0x0118); // �
	b.AddChar(0x0119); // �
	b.AddChar(0x0141); // �
	b.AddChar(0x0142); // �
	b.AddChar(0x0143); // �
	b.AddChar(0x0144); // �
	b.AddChar(0x00d3); // �
	b.AddChar(0x00f3); // �
	b.AddChar(0x015a); // �
	b.AddChar(0x015b); // �
	b.AddChar(0x0179); // �
	b.AddChar(0x017a); // �
	b.AddChar(0x017b); // �
	b.AddChar(0x017c); // �

	ImVector<ImWchar> range;
	b.BuildRanges(&range);

	io.Fonts->AddFontFromFileTTF("fonts/roboto/Roboto-Regular.ttf", 16.0f, NULL, range.Data);
	
    // Upload Fonts

	// Use any command queue
	VkCommandPool command_pool = mainWindowData.Frames[mainWindowData.FrameIndex].CommandPool;
	VkCommandBuffer command_buffer = mainWindowData.Frames[mainWindowData.FrameIndex].CommandBuffer;

	VkResult err = vkResetCommandPool(device, command_pool, 0);
	CheckVkResult(err);
	VkCommandBufferBeginInfo begin_info = {};
	begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	begin_info.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	err = vkBeginCommandBuffer(command_buffer, &begin_info);
	CheckVkResult(err);

	ImGui_ImplVulkan_CreateFontsTexture(command_buffer);

	VkSubmitInfo end_info = {};
	end_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	end_info.commandBufferCount = 1;
	end_info.pCommandBuffers = &command_buffer;
	err = vkEndCommandBuffer(command_buffer);
	CheckVkResult(err);
	err = vkQueueSubmit(queue, 1, &end_info, VK_NULL_HANDLE);
	CheckVkResult(err);

	err = vkDeviceWaitIdle(device);
	CheckVkResult(err);
	ImGui_ImplVulkan_DestroyFontUploadObjects();
}

void VulkanLoader::NewFrame(GLFWwindow* window)
{
	if (swapchainRebuild)
	{
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		if (width > 0 && height > 0)
		{
			ImGui_ImplVulkan_SetMinImageCount(minImageCount);
			ImGui_ImplVulkanH_CreateOrResizeWindow(instance, physicalDevice, device, &mainWindowData, queueFamily, allocator, width, height, minImageCount);
			mainWindowData.FrameIndex = 0;
			swapchainRebuild = false;
		}
	}

	ImGui_ImplVulkan_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
}

void VulkanLoader::Render()
{
	ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
	ImGui::Render();
	ImDrawData* draw_data = ImGui::GetDrawData();
	const bool is_minimized = (draw_data->DisplaySize.x <= 0.0f || draw_data->DisplaySize.y <= 0.0f);
	if (!is_minimized)
	{
		memcpy(&mainWindowData.ClearValue.color.float32[0], &clear_color, 4 * sizeof(float));
		RenderFrame(draw_data);
		PresentFrame();
	}
}

void VulkanLoader::RenderFrame(ImDrawData* draw_data)
{
	VkResult err;

	VkSemaphore image_acquired_semaphore = mainWindowData.FrameSemaphores[mainWindowData.SemaphoreIndex].ImageAcquiredSemaphore;
	VkSemaphore render_complete_semaphore = mainWindowData.FrameSemaphores[mainWindowData.SemaphoreIndex].RenderCompleteSemaphore;
	err = vkAcquireNextImageKHR(device, mainWindowData.Swapchain, UINT64_MAX, image_acquired_semaphore, VK_NULL_HANDLE, &mainWindowData.FrameIndex);
	if (err == VK_ERROR_OUT_OF_DATE_KHR)
	{
		swapchainRebuild = true;
		return;
	}
	CheckVkResult(err);

	ImGui_ImplVulkanH_Frame* fd = &mainWindowData.Frames[mainWindowData.FrameIndex];
	
	err = vkWaitForFences(device, 1, &fd->Fence, VK_TRUE, UINT64_MAX);
	CheckVkResult(err);

	err = vkResetFences(device, 1, &fd->Fence);
	CheckVkResult(err);
	
	err = vkResetCommandPool(device, fd->CommandPool, 0);
	CheckVkResult(err);
	VkCommandBufferBeginInfo commandBufferInfo = {};
	commandBufferInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	commandBufferInfo.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	err = vkBeginCommandBuffer(fd->CommandBuffer, &commandBufferInfo);
	CheckVkResult(err);
	
	VkRenderPassBeginInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassInfo.renderPass = mainWindowData.RenderPass;
	renderPassInfo.framebuffer = fd->Framebuffer;
	renderPassInfo.renderArea.extent.width = mainWindowData.Width;
	renderPassInfo.renderArea.extent.height = mainWindowData.Height;
	renderPassInfo.clearValueCount = 1;
	renderPassInfo.pClearValues = &mainWindowData.ClearValue;
	vkCmdBeginRenderPass(fd->CommandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
	
	ImGui_ImplVulkan_RenderDrawData(draw_data, fd->CommandBuffer);

	vkCmdEndRenderPass(fd->CommandBuffer);

	VkPipelineStageFlags wait_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = &image_acquired_semaphore;
	submitInfo.pWaitDstStageMask = &wait_stage;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &fd->CommandBuffer;
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = &render_complete_semaphore;

	err = vkEndCommandBuffer(fd->CommandBuffer);
	CheckVkResult(err);
	err = vkQueueSubmit(queue, 1, &submitInfo, fd->Fence);
	CheckVkResult(err);
}

void VulkanLoader::PresentFrame()
{
	if (swapchainRebuild)
		return;
	VkSemaphore render_complete_semaphore = mainWindowData.FrameSemaphores[mainWindowData.SemaphoreIndex].RenderCompleteSemaphore;
	VkPresentInfoKHR info = {};
	info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	info.waitSemaphoreCount = 1;
	info.pWaitSemaphores = &render_complete_semaphore;
	info.swapchainCount = 1;
	info.pSwapchains = &mainWindowData.Swapchain;
	info.pImageIndices = &mainWindowData.FrameIndex;
	VkResult err = vkQueuePresentKHR(queue, &info);
	if (err == VK_ERROR_OUT_OF_DATE_KHR)
	{
		swapchainRebuild = true;
		return;
	}
	CheckVkResult(err);
	mainWindowData.SemaphoreIndex = (mainWindowData.SemaphoreIndex + 1) % mainWindowData.ImageCount;
}

void VulkanLoader::Cleanup()
{
	ResourcesManager::Cleanup();
	TexturesData::Cleanup();
	CommandPool::cleanup();

	ImGui_ImplVulkan_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	CleanupVulkanWindow();

	vkDestroyDescriptorPool(device, descriptorPool, allocator);

	if constexpr (debug) {
		auto vkDestroyDebugReportCallbackEXT = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
		vkDestroyDebugReportCallbackEXT(instance, debugReport, allocator);
	}

	vkDestroyDevice(device, allocator);
	vkDestroyInstance(instance, allocator);
}

VkDevice VulkanLoader::GetDevice()
{
	return device;
}

void VulkanLoader::CreateVulkanInstance(const char** extensions, uint32_t extensionsCount)
{
	VkResult err;

	VkInstanceCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.enabledExtensionCount = extensionsCount;
	createInfo.ppEnabledExtensionNames = extensions;

	if constexpr(debug) {
		const char* layers[] = { "VK_LAYER_KHRONOS_validation" };
		createInfo.enabledLayerCount = 1;
		createInfo.ppEnabledLayerNames = layers;

		const char** extensionsExt = (const char**)malloc(sizeof(const char*) * (extensionsCount + 1));
		memcpy(extensionsExt, extensions, extensionsCount * sizeof(const char*));
		extensionsExt[extensionsCount] = "VK_EXT_debug_report";
		createInfo.enabledExtensionCount = extensionsCount + 1;
		createInfo.ppEnabledExtensionNames = extensionsExt;

		err = vkCreateInstance(&createInfo, allocator, &instance);
		CheckVkResult(err);
		free(extensionsExt);

		auto vkCreateDebugReportCallbackEXT = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
		IM_ASSERT(vkCreateDebugReportCallbackEXT != NULL);

		VkDebugReportCallbackCreateInfoEXT debugReportInfo{};
		debugReportInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
		debugReportInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
		debugReportInfo.pfnCallback = DebugReport;
		debugReportInfo.pUserData = NULL;
		err = vkCreateDebugReportCallbackEXT(instance, &debugReportInfo, allocator, &debugReport);
		CheckVkResult(err);
	}
	else {
		err = vkCreateInstance(&createInfo, allocator, &instance);
		CheckVkResult(err);
		IM_UNUSED(debugReport);
	}
}

void VulkanLoader::SelectGPU()
{
	VkResult err;

	uint32_t gpuCount;
	err = vkEnumeratePhysicalDevices(instance, &gpuCount, nullptr);
	CheckVkResult(err);
	IM_ASSERT(gpuCount > 0);

	VkPhysicalDevice* gpus = (VkPhysicalDevice*)malloc(sizeof(VkPhysicalDevice) * gpuCount);
	err = vkEnumeratePhysicalDevices(instance, &gpuCount, gpus);
	CheckVkResult(err);

	physicalDevice = gpus[0];
	free(gpus);
}

void VulkanLoader::SelectGraphicsQueueFamily()
{
	uint32_t count;
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &count, NULL);
	VkQueueFamilyProperties* queues = (VkQueueFamilyProperties*)malloc(sizeof(VkQueueFamilyProperties) * count);
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &count, queues);
	for (uint32_t i = 0; i < count; i++)
		if (queues[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
		{
			queueFamily = i;
			break;
		}
	free(queues);
	IM_ASSERT(queueFamily != (uint32_t)-1);
}

void VulkanLoader::CreateLogicalDevice()
{
	VkResult err;
	int device_extension_count = 1;
	const char* device_extensions[] = { "VK_KHR_swapchain" };
	const float queue_priority[] = { 1.0f };
	VkDeviceQueueCreateInfo queue_info[1] = {};
	queue_info[0].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	queue_info[0].queueFamilyIndex = queueFamily;
	queue_info[0].queueCount = 1;
	queue_info[0].pQueuePriorities = queue_priority;
	VkDeviceCreateInfo create_info = {};
	create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	create_info.queueCreateInfoCount = sizeof(queue_info) / sizeof(queue_info[0]);
	create_info.pQueueCreateInfos = queue_info;
	create_info.enabledExtensionCount = device_extension_count;
	create_info.ppEnabledExtensionNames = device_extensions;
	err = vkCreateDevice(physicalDevice, &create_info, allocator, &device);
	CheckVkResult(err);
	vkGetDeviceQueue(device, queueFamily, 0, &queue);
}

void VulkanLoader::CreateDescriptorPool()
{
	VkResult err;
	VkDescriptorPoolSize pool_sizes[] =
	{
		{ VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
		{ VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000 },
		{ VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000 },
		{ VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
	};
	VkDescriptorPoolCreateInfo pool_info = {};
	pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
	pool_info.maxSets = 1000 * IM_ARRAYSIZE(pool_sizes);
	pool_info.poolSizeCount = (uint32_t)IM_ARRAYSIZE(pool_sizes);
	pool_info.pPoolSizes = pool_sizes;
	err = vkCreateDescriptorPool(device, &pool_info, allocator, &descriptorPool);
	CheckVkResult(err);
}

void CheckVkResult(VkResult err)
{
	if (err == 0)
		return;

	fprintf(stderr, "[Vulkan] Error: VkResult = %d\n", err);
	if (err < 0)
		abort();
}

void VulkanLoader::SetupVulkanWindow(ImGui_ImplVulkanH_Window* wd, VkSurfaceKHR surface, int width, int height)
{
	wd->Surface = surface;

	VkBool32 res;
	vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, queueFamily, wd->Surface, &res);
	if (res != VK_TRUE)
	{
		fprintf(stderr, "Error no WSI support on physical device 0\n");
		exit(-1);
	}

	const VkFormat requestSurfaceImageFormat[] = { VK_FORMAT_B8G8R8A8_UNORM, VK_FORMAT_R8G8B8A8_UNORM, VK_FORMAT_B8G8R8_UNORM, VK_FORMAT_R8G8B8_UNORM };
	const VkColorSpaceKHR requestSurfaceColorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
	wd->SurfaceFormat = ImGui_ImplVulkanH_SelectSurfaceFormat(physicalDevice, wd->Surface, requestSurfaceImageFormat, (size_t)IM_ARRAYSIZE(requestSurfaceImageFormat), requestSurfaceColorSpace);

	VkPresentModeKHR present_modes[] = { VK_PRESENT_MODE_FIFO_KHR };

	wd->PresentMode = ImGui_ImplVulkanH_SelectPresentMode(physicalDevice, wd->Surface, &present_modes[0], IM_ARRAYSIZE(present_modes));

	IM_ASSERT(minImageCount >= 2);
	ImGui_ImplVulkanH_CreateOrResizeWindow(instance, physicalDevice, device, wd, queueFamily, allocator, width, height, minImageCount);
}

void VulkanLoader::CleanupVulkanWindow()
{
	ImGui_ImplVulkanH_DestroyWindow(instance, device, &mainWindowData, allocator);
}

VKAPI_ATTR VkBool32 VKAPI_CALL DebugReport(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objectType, uint64_t object, size_t location, int32_t messageCode, const char* pLayerPrefix, const char* pMessage, void* pUserData)
{
	fprintf(stderr, "[vulkan] Debug report from ObjectType: %i\nMessage: %s\n\n", objectType, pMessage);
	return VK_FALSE;
}

uint32_t VulkanLoader::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties)
{
	VkPhysicalDeviceMemoryProperties memProperties;
	vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);

	for (uint32_t i = 0; i < memProperties.memoryTypeCount; ++i) {
		if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
			return i;
		}
	}
	return (uint32_t)-1;
}

uint32_t VulkanLoader::GetQueueFamilyIndex()
{
	return queueFamily;
}

VkQueue VulkanLoader::GetQueue()
{
	return queue;
}
