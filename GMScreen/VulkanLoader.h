#pragma once

#include "ImGUI/imgui.h"
#include "ImGUI/imgui_impl_glfw.h"
#include "ImGUI/imgui_impl_vulkan.h"

#include <stdio.h>
#include <stdlib.h>

#define GLFW_INCLUDE_NONE
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

VKAPI_ATTR VkBool32 VKAPI_CALL DebugReport(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objectType, uint64_t object, size_t location, int32_t messageCode, const char* pLayerPrefix, const char* pMessage, void* pUserData);
void CheckVkResult(VkResult err);

class VulkanLoader
{
public:
	static void Setup(GLFWwindow* window, const char** extensions, uint32_t extensionsCount);

	static void InitImGui(GLFWwindow* window);

	static void LoadFonts(ImGuiIO& io);

	static void NewFrame(GLFWwindow* window);

	static void Render();

	static void Cleanup();

	static VkDevice GetDevice();
	static uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);
	static uint32_t GetQueueFamilyIndex();
	static VkQueue GetQueue();

private:
	VulkanLoader() = delete;

	static void CreateVulkanInstance(const char** extensions, uint32_t extensionsCount);
	static void SelectGPU();
	static void SelectGraphicsQueueFamily();
	static void CreateLogicalDevice();
	static void CreateDescriptorPool();

	static void RenderFrame(ImDrawData* draw_data);
	static void PresentFrame();

	static void SetupVulkanWindow(ImGui_ImplVulkanH_Window* wd, VkSurfaceKHR surface, int width, int height);
	static void CleanupVulkanWindow();

	static VkAllocationCallbacks* allocator;
	static VkInstance instance;
	static VkPhysicalDevice physicalDevice;
	static VkDevice device;

	static uint32_t queueFamily;
	static VkQueue queue;

	static VkDebugReportCallbackEXT debugReport;
	
	static VkPipelineCache pipelineCache;
	static VkDescriptorPool descriptorPool;

	static ImGui_ImplVulkanH_Window mainWindowData;

	static int minImageCount;
	static bool swapchainRebuild;

#ifdef _DEBUG
	static const bool debug = true;
#else
	static const bool debug = false;
#endif
};

