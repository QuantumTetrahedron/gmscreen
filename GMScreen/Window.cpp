#include "Window.h"
#include "WindowManager.h"

Window::Window(std::string name, int uid)
	: name(name), uid(uid) {
	open = true;
}

std::string Window::GetInternalName() {
	return name + "##" + name + std::to_string(uid);
}

void Window::Draw() {
    if (!open) {
        WindowManager::CloseWindow(shared_from_this());
        return;
    }

    ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 1.0f);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(8.0f, 8.0f));

    std::string fullName = GetInternalName();

    if (startDocked) {
        ImGui::SetNextWindowDockID(WindowManager::GetMainDockId(), ImGuiCond_Appearing);
    }

    if (ImGui::Begin(fullName.c_str(), &open, flags)) {
        DrawContent();
    }
    ImGui::End();

    ImGui::PopStyleVar(2);
}

void Window::Focus()
{
    ImGui::SetWindowFocus(GetInternalName().c_str());
}

void Window::SetWindowFlags(ImGuiWindowFlags _flags)
{
    flags = _flags;
}

ImGuiWindowFlags Window::GetWindowFlags()
{
    return flags;
}
