#pragma once

#include <memory>
#include <vector>
#include "ImGUI/imgui.h"
#include <string>

class Window : public std::enable_shared_from_this<Window>
{
public:
	Window(std::string name, int uid);

	virtual void DrawContent() = 0;
	virtual void ProcessIO(ImGuiIO& io) = 0;
	virtual void Cleanup() {}
	virtual ~Window() {}

	int GetUID() { return uid; }
	std::string GetName() { return name; }

	void Draw();
	void Focus();

	void SetWindowFlags(ImGuiWindowFlags flags);
	ImGuiWindowFlags GetWindowFlags();
protected:
	std::string name;
	int uid;

	friend class WindowManager;
	std::string GetInternalName();

	bool open;

	ImGuiWindowFlags flags = 0;

	bool startDocked = false;
	virtual void OnFileLoad() {}
};

