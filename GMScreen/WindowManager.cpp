#include "WindowManager.h"

#include "Workspace.h"
#include "Properties.h"
#include "MapsBrowser.h"
#include "PlayersBrowser.h"
#include <iostream>
#include "ImGUI/imgui_internal.h"

std::vector<std::shared_ptr<Window>> WindowManager::windows;
TopMenu WindowManager::topMenu;
std::map<std::string, std::vector<int>> WindowManager::windowInstancesOpen;
std::vector<std::shared_ptr<Window>> WindowManager::toClose;
std::vector<std::shared_ptr<Window>> WindowManager::toOpen;

void WindowManager::Init()
{
    //OpenWindow<Workspace>("Workspace");
    OpenWindow<Properties>("Properties");
    OpenWindow<MapsBrowser>("Maps Browser");
    OpenWindow<PlayersBrowser>("Players");
    //windows.push_back(std::make_shared<Data>());
    //windows.push_back(std::make_shared<Log>());
}

void WindowManager::Draw()
{
    for (std::shared_ptr<Window> name : toOpen) {
        OpenWindowInternal(name);
    }
    toOpen.clear();

    topMenu.Draw();

    //ImGui::ShowDemoWindow();

    ImGuiViewport* viewport = ImGui::GetMainViewport();
    ImGui::SetNextWindowPos(viewport->WorkPos);
    ImGui::SetNextWindowSize(viewport->WorkSize);
    ImGui::SetNextWindowViewport(viewport->ID);

    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));

    ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDocking;
    window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

    ImGui::Begin("Docker", nullptr, window_flags);

    ImGuiID dockspaceID = GetMainDockId();

    if (!ImGui::DockBuilderGetNode(dockspaceID)) {
        ImGui::DockBuilderRemoveNode(dockspaceID);
        ImGui::DockBuilderAddNode(dockspaceID, ImGuiDockNodeFlags_DockSpace);
        ImGui::DockBuilderSetNodeSize(dockspaceID, ImVec2(800, 800));

        ImGuiID dock_main_id = dockspaceID;

        ImGuiID dock_left_id = ImGui::DockBuilderSplitNode(dock_main_id, ImGuiDir_Left, 0.5f, nullptr, &dock_main_id);
        ImGuiID dock_right_id = ImGui::DockBuilderSplitNode(dock_main_id, ImGuiDir_Right, 0.9f, nullptr, &dock_main_id);
        //ImGuiID dock_down_id = ImGui::DockBuilderSplitNode(dock_main_id, ImGuiDir_Down, 0.2f, nullptr, &dock_main_id);

        //ImGui::DockBuilderDockWindow("Data", dock_left_id);
        std::shared_ptr<MapsBrowser> mapsBrowser = GetWindow<MapsBrowser>();
        if (mapsBrowser) {
            ImGui::DockBuilderDockWindow(mapsBrowser->GetInternalName().c_str(), dock_left_id);
        }

        std::shared_ptr<Properties> properties = GetWindow<Properties>();
        if (properties) {
            ImGui::DockBuilderDockWindow(properties->GetInternalName().c_str(), dock_right_id);
        }

        std::shared_ptr<PlayersBrowser> players = GetWindow<PlayersBrowser>();
        if (players) {
            ImGui::DockBuilderDockWindow(players->GetInternalName().c_str(), dock_left_id);
        }

        //ImGui::DockBuilderDockWindow("Log", dock_down_id);

        ImGui::DockBuilderFinish(dock_main_id);
    }
    ImGui::DockSpace(dockspaceID, ImVec2(0, 0), ImGuiDockNodeFlags_NoWindowMenuButton | ImGuiDockNodeFlags_NoCloseButton);

    int dockedFlags = ImGuiWindowFlags_NoMove;

    //ImGui::SetNextWindowDockID(dockspaceID, ImGuiCond_Appearing);

    for (auto& w : windows) {
        w->Draw();
    }

    ImGui::End();
    ImGui::PopStyleVar(3);

    CleanupClosedWindows();
}

void WindowManager::ProcessIO(ImGuiIO& io)
{
    for (auto& w : windows) {
        w->ProcessIO(io);
    }
}

