#pragma once

#include <vector>
#include <memory>
#include "Window.h"
#include "TopMenu.h"

class WindowManager
{
public:
	WindowManager() = delete;

	static void Init();

	static void Draw();
	static void ProcessIO(ImGuiIO& io);

	template<typename T>
	static void OpenWindow(std::string name, bool focusIfOpened = true, bool dock = false) {
		if (windowInstancesOpen.find(name) == windowInstancesOpen.end()) {
			windowInstancesOpen[name] = std::vector<int>();
		}

		std::vector<int>& vec = windowInstancesOpen[name];

		if (!vec.empty() && focusIfOpened) {
			int uid = vec.back();

			for (auto& w : windows) {
				if (w->GetName() == name && w->GetUID() == uid) {
					w->Focus();
					break;
				}
			}
			return;
		}

		int i = 0;
		for (; i < vec.size(); ++i) {
			if (std::find(vec.begin(), vec.end(), i) == vec.end()) {
				break;
			}
		}

		windowInstancesOpen[name].push_back(i);
		toOpen.push_back(std::make_shared<T>(name, i));
	}

	static void CloseWindow(std::shared_ptr<Window> window) {
		toClose.push_back(window);
	}

	static void Cleanup() {
		for (auto& window : windows) {
			window->Cleanup();
		}
	}

	template <typename T>
	static std::shared_ptr<T> GetWindow(std::string windowName = "") {
		for (std::shared_ptr<Window>& window : windows) {
			auto w = std::dynamic_pointer_cast<T>(window);
			if (w && (windowName.empty() || windowName == w->GetName())) {
				return w;
			}
		}
		return nullptr;
	}

	static ImGuiID GetMainDockId() {
		return ImGui::GetID("MyDockSpace");
	}

	static void OnFileLoad() {
		for (auto& window : windows) {
			window->OnFileLoad();
		}
	}
private:
	static std::vector<std::shared_ptr<Window>> windows;
	static TopMenu topMenu;

	static std::map<std::string, std::vector<int>> windowInstancesOpen;

	static std::vector<std::shared_ptr<Window>> toOpen;
	static std::vector<std::shared_ptr<Window>> toClose;

	static void CleanupClosedWindows() {
		for (auto& window : toClose) {
			std::string name = window->GetName();
			int id = window->GetUID();
			windows.erase(std::remove_if(std::begin(windows), std::end(windows), [&](auto& w) {
				return w->GetUID() == window->GetUID() && w->GetName() == window->GetName();
				}), std::end(windows));
			auto& vec = windowInstancesOpen[name];
			vec.erase(std::remove(vec.begin(), vec.end(), id), vec.end());
		}
		toClose.clear();
	}

	static void OpenWindowInternal(std::shared_ptr<Window> w) {
		windows.push_back(w);
	}
};

