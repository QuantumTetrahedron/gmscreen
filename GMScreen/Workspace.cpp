#include "Workspace.h"
#include <algorithm>
#include <iostream>

#include "WindowManager.h"

const int DELETE_KEY_CODE = 261;

Workspace::Workspace(std::string name, int uid) : Window(name, uid) {
    workspaceOffset = glm::vec2(200, 200);
}

void Workspace::DrawContent() {
    ImDrawList* draw_list = ImGui::GetWindowDrawList();
    hovered = ImGui::IsWindowHovered();

    if (ImGui::IsWindowHovered() && ImGui::IsMouseReleased(ImGuiMouseButton_Right) && shouldOpenContextMenu) {
        ImGui::OpenPopup("contextMenu");
        shouldOpenContextMenu = false;
    }
    if (ImGui::BeginPopup("contextMenu")) {
        ImGui::Text("Context menu");
        ImGui::EndPopup();
    }
}

void Workspace::ProcessIO(ImGuiIO& io) {
    if (!hovered) {
        return;
    }
    mousePos = glm::vec2(io.MousePos.x, io.MousePos.y) - workspaceOffset;

    switch (state) {
    case State::idle:
        IdleIO(io);
        break;
    case State::viewDrag:
        ViewDragIO(io);
        break;
    default:
        break;
    }
}

void Workspace::IdleIO(ImGuiIO& io)
{
    if (io.MouseClicked[1]) {
        shouldOpenContextMenu = true;
    }

    if (io.MouseClicked[2]) {
        glm::vec2 clickPos = glm::vec2(io.MouseClickedPos[2].x, io.MouseClickedPos[2].y);
        state = State::viewDrag;
        dragOffset = clickPos - workspaceOffset;
    }
}

void Workspace::ViewDragIO(ImGuiIO& io)
{
    glm::vec2 mousePos = glm::vec2(io.MousePos.x, io.MousePos.y);
    workspaceOffset = mousePos - dragOffset;

    if (io.MouseReleased[2]) {
        state = State::idle;
    }
}