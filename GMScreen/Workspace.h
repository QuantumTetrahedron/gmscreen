#pragma once
#include "Window.h"
#include <glm/glm.hpp>

class Workspace : public Window
{
public:
	Workspace(std::string name, int uid);
	void DrawContent() override;
	void ProcessIO(ImGuiIO& io) override;

private:
	glm::vec2 workspaceOffset;

	glm::vec2 dragOffset = glm::vec2(0.0f);
	glm::vec2 mousePos = glm::vec2(0.0f);
	bool hovered = false;
	bool shouldOpenContextMenu = false;

	enum class State {
		idle, viewDrag
	};
	State state = State::idle;

	void IdleIO(ImGuiIO& io);
	void ViewDragIO(ImGuiIO& io);
};

