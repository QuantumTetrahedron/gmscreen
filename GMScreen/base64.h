#pragma once

#include <vector>
#include <string>

namespace base64 {
	typedef unsigned char BYTE;

	std::string encode(BYTE const* buf, unsigned int bufLen);
	std::vector<BYTE> decode(std::string const&);
}
