-- sample character sheet for a non-existent RPG system

-- draws a character attribute with a calculated modifier to potencial die rolls
function DrawAttribute(name)
	-- output attribute name
	text(name);

	-- attribute value field
	-- add ## to identifier to not display it next to the field
	n = "##"..name;
	--		 id, default value, drag speed, min, max
	inputInt(n,  10,			0.05,		1,	 30);

	-- roll modifier equal to attribute - 10
	mod = math.floor(intValue(n) - 10);

	-- display the modifier in a smaller box next to the attribute
	sameLine();
	setNextItemWidth(35.0);

	-- generate a unique id for this modifier
	n = n.."mod";

	-- displayInt to make it readonly
	displayInt(n, mod, "%+d");
end

-- draws 6 character attributes in 3 columns
function DrawAttributes()
	w, h = getContentRegion();
	w = w * 0.3;
	beginChild("attributesL", w, 100);
		DrawAttribute("Attr 1");
		DrawAttribute("Attr 4");
	endChild();
	
	sameLine();

	beginChild("attributesM", w, 100);
		DrawAttribute("Attr 2");
		DrawAttribute("Attr 5");
	endChild();

	sameLine();
	
	beginChild("attributesR", w, 100);
		DrawAttribute("Attr 3");
		DrawAttribute("Attr 4");
	endChild();
end

-- Draws a list of character features.
-- Each feature can have a name and a multiline description.
-- Displays a list of features on the left size of the area.
-- Clicking on a feature displays its full description
function DrawFeatures()
	-- currently selected feature from the list (returns 0 if int was not set before)
	selectedFeature = getInt("##selected_feature");
	
	-- feature list
	beginChild("Features List", 200, 300, true);
	size = vecSize("##features")
	for i=0,size-1 do
		featureId = "##feature_"..i;
		featureName = vecGetText("##features", i);
		if selectable(featureName..featureId, (selectedFeature - 1) == i) then
			selectedFeature = i+1;
			setInt("##selected_feature", selectedFeature);
		end
	end
	if button("+##add_feature") then
		vecAddText("##features", "New Feature")
		vecAddText("##features_desc", "");
	end
	endChild();

	-- selected feature description and button to remove the feature
	if selectedFeature > 0 and selectedFeature <= size then
		featureIndex = selectedFeature - 1;
		sameLine();
		beginChild("Feature View", 0, 300);
		inputVecText("##features", featureIndex);
		inputVecTextMultiline("##features_desc", featureIndex, 0, 247);

		if button("Remove##remove_feature") then
			vecErase("##features", featureIndex);
			vecErase("##features_desc", featureIndex);
			setInt("##selected_feature", 0);
		end
		endChild();
	end
end

-- equipment, similar to features
function DrawEquipment()
	selectedItem = getInt("##selected_item");
	beginChild("Items List", 200, 300, true);
	size = vecSize("##items")
	for i=0,size-1 do
		itemId = "##item_"..i;
		itemName = vecGetText("##items", i);
		if selectable(itemName..itemId, (selectedItem - 1) == i) then
			selectedItem = i+1;
			setInt("##selected_item", selectedItem);
		end
	end
	if button("+##add_item") then
		vecAddText("##items", "New Item")
		vecAddText("##items_desc", "");
	end
	endChild();

	if selectedItem > 0 and selectedItem <= size then
		itemIndex = selectedItem - 1;
		sameLine();
		beginChild("Items View", 0, 300);
		inputVecText("##items", itemIndex);
		inputVecTextMultiline("##items_desc", itemIndex, 0, 247);

		if button("Remove##remove_item") then
			vecErase("##items", featureIndex);
			vecErase("##items_desc", featureIndex);
			setInt("##selected_item", 0);
		end
		endChild();
	end
end

-- main

inputText("Origin");
inputText("Profession");
inputInt("Level", 1, 0.05, 1, 20);
combo("Alignment", 4, 9, "Lawful Good", "Neutral Good", "Chaotic Good", "Lawful Neutral", "Neutral", "Chaotic Neutral", "Lawful Evil", "Neutral Evil", "Chaotic Evil");

separator();

w, h = getContentRegion();
w = w * 0.16;

beginChild("##health", w, 50);
	text("Health Points");
	setNextItemWidth(w);
	inputInt("##hitPoints", 1, 0.05, 0, 0, "%d HP");
endChild();

sameLine();

beginChild("##sanity", w, 50);
	text("Sanity Points");
	setNextItemWidth(w);
	inputInt("##armorClass", 1, 0.05, 0, 0, "%d SP");
endChild();

sameLine();

beginChild("##initiative", w, 50);
	text("Initiative");
	setNextItemWidth(w);
	inputInt("##initiative", 0, 0.05, 0, 0, "%+d");
endChild();

sameLine();

beginChild("##speed", w, 50);
	text("Speed");
	setNextItemWidth(w);
	inputInt("##speed", 20, 0.05, 0, 0, "%d feet");
endChild();

if collapsingHeader("Attributes") then
	DrawAttributes();
end

if collapsingHeader("Features") then
	DrawFeatures();
end

if collapsingHeader("Equipment") then
	DrawEquipment();
end