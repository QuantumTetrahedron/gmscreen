# GM-Screen

Simple program for game masters to help with session planning and running.

## Features

* Import pngs with maps, make notes on them by adding descriptions to fields
* Setup areas on the map to be hidden/revealed to the players as they explore
* Store the player data that you need with character sheets generated from Lua scripts
* Make a list of NPCs, with an image, a description and a list of tags to search through them
* The NPCs window should probably be just called database, just put anything you want there!

## TO-DO

* [In Progress] Charts, simple graphs to model the adventure flowchart
* Tables, to store the data of that weird plant gathering homebrew subsystem or whatever
* Soundboard to easilly switch the background music or play a door opening sound effect
* A system to allow linking a map place description to the NPC entry in that location
* probably more stuff someday
